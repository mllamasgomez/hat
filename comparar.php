﻿<?php
//2016 - Manuel Llamas Gómez - Pool de implantación
session_start();
include("BaseDatos.php");
$nombreFicheroOld=$_SESSION['ficheroold'];//Nombre del fichero antiguo xml
$nombreFicheroNew=$_SESSION['ficheronew']; //Nombre del fichero nuevo xml
$chkMin=$_SESSION['chkmin'];//Check de quitar
$chkNew=$_SESSION['chkadd'];//Check de aniadir
$_SESSION['enviado']=true;

//echo "check de añadir:".$chkNew;
//echo "</br>";
//echo "check de quitar:".$chkMin;

//echo "Fichero antiguo:".$nombreFicheroOld;
//echo "Fichero nuevo:".$nombreFicheroNew;

$xml = simplexml_load_file("tmp/".$nombreFicheroNew);
error_reporting(E_ALL ^ E_NOTICE);
$xmlold = simplexml_load_file("tmp/".$nombreFicheroOld);
error_reporting(E_ALL ^ E_NOTICE);

$numwsexp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
$numwscon=count($xml->webServices->webServiceStates->webServiceState);//Numero de ws consumidos
$numtrxop=count($xml->communication->trxOp->trxOpStates->trxOpState);//Numero de transacciones partenon
$numsat = count($xml->communication->sat->satStates->satState);//Numero de sat
$numtrxalt=count($xml->communication->altair->altairStates->altairState);//Numero de transacciones altair
$ensnew=$xml->assembly['name'];
$ensold=$xmlold->assembly['name'];

//Validaciones iniciales
if (trim($ensnew)!=trim($ensold)) echo '<script language="javascript">alert("¡Estás comparando dos ensamblados diferentes!");</script>'; //Si se comparan distintos ens
$result = xml_is_equal($xml,$xmlold);
if ($result === true) {
	echo "No ha habido ningun cambio de configuración ntre las dos publicaciones.";
	delfichtemp($nombreFicheroNew,$nombreFicheroOld);
}else{
	pintarTitulo($xml->assembly['name']);
	wsExpuestos($xml,$xmlold,$chkMin,$chkNew);//Web services expuestos
	wsConsumidos($xml,$xmlold,$chkMin,$chkNew);//Web services consumidos
	tablasParametros($xml,$xmlold,$conexion,$nombreFicheroNew,$chkMin,$chkNew);//Tablas de parámetros
	trxOP($xml,$xmlold,$chkMin,$chkNew);//TrxOP
	perLocalizadores($xml,$xmlold,$conexion,$doc,$nombreFicheroNew,$chkMin,$chkNew);//Tabla localizadores
	catalogoContenidos($xml,$xmlold);//Catálogo de contenidos
	deltabletemp($conexion);
	delfichtemp($nombreFicheroNew,$nombreFicheroOld);
	}

function deltabletemp($conexion){
//Borrado de tablas
deltetbtmp("localizador",$conexion);
deltetbtmp("consultas",$conexion);
deltetbtmp("agrupacion",$conexion);
deltetbtmp("agrupacion_tmp",$conexion);
deltetbtmp("agrup_perm_old",$conexion);
deltetbtmp("agrup_perm_new",$conexion);	
deltetbtmp("ttpp_new",$conexion);	
deltetbtmp("ttpp_old",$conexion);	
deletetmpttpp($conexion);
}

function delfichtemp($nombreFicheroNew,$nombreFicheroOld){
//Borrado de ficheros temporales
$ifile ="/isban/tmp/".$nombreFicheroNew; // this is the actual path to the file you want to delete.
//echo $ifile;
unlink($_SERVER['DOCUMENT_ROOT'] .$ifile);
$ifile ="/isban/tmp/".$nombreFicheroOld; // this is the actual path to the file you want to delete.
//echo $ifile;
unlink($_SERVER['DOCUMENT_ROOT'] .$ifile);
}

function deltetbtmp ($tabla,$conexion) {
	$consulta = "TRUNCATE TABLE ".$tabla;
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "$tabla";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "$tabla";
		print "existe cursor";
		print "$consulta";
	}
}

//Titulo
function pintarTitulo($nomens){
	echo"<br/>";
    echo "<font color = \"0B0B3B\" SIZE=5 ALIGN = \"center\" ><b>Comparador ".$nomens."</b></font>"; 
	echo"<br/>";
	echo"<br/>";
}

function perLocalizadores($xml,$xmlold,$conexion,$doc,$nombreFicheroNew,$chkMin,$chkNew){

$sqlNew=$xml->sqlComponents;
$sqlOld=$xmlold->sqlComponents;
	
$result = xml_is_equal($sqlNew,$sqlOld);
if ($result === true) {
// Si son iguales
//echo "</br>";//Para dejar espacio
//echo"<td><font color = \"0B0B3B\"><b>Permisos Localizadores</b></font> - Sin cambios</td>";
} else {

//Se añaden
perLocNew($xml,$conexion,$nomFichpro);
perLocOld($xmlold,$conexion,$nomFichpro);

echo "<table></table>";

if ($chkNew){
$consultaNew="SELECT tabla,permiso,localizador FROM agrup_perm_new WHERE NOT EXISTS (SELECT * FROM agrup_perm_old WHERE agrup_perm_new.tabla=agrup_perm_old.tabla)";
$cursorNew=mysqli_query($conexion,$consultaNew);
if(!$cursorNew) {
//print "alta";
}
else {
	$n=mysqli_num_rows($cursorNew);
	if ($n>0){
		echo "<br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Accesos SQL Añadidos</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		. "<thead>"
		. "<TR>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
		. "</TR>"
		. "</thead>"
		. "<tbody>";

		while($datosNew=mysqli_fetch_array($cursorNew)){
		$tabla=$datosNew[0];
		$permiso=$datosNew[1];
		$localizador=$datosNew[2];
		echo "<tr>";
		echo "<td>$tabla</td>";
		echo "<td>$permiso</td>";
		echo "<td>$localizador</td>";
		echo "</tr>";
		}
		
		echo "</tbody></table></tr></td></table></br>";
	}	
	
}
}

echo "<table></table>";	
//Se quitan

if($chkMin){
$consultaOld="SELECT tabla,permiso,localizador FROM agrup_perm_old WHERE NOT EXISTS (SELECT * FROM agrup_perm_new WHERE agrup_perm_new.tabla=agrup_perm_old.tabla)";
$cursorOld=mysqli_query($conexion,$consultaOld);
if(!$cursorOld) {
//print "alta";
}
else {
	$n=mysqli_num_rows($cursorOld);
	if ($n>0){
//Se añaden
//Cabecera
		echo "</br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Accesos SQL Eliminados</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		. "<thead>"
		. "<TR>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
		. "</TR>"
		. "</thead>"
		. "<tbody>";

while($datosOld=mysqli_fetch_array($cursorOld)){
		$tabla=$datosOld[0];
		$permiso=$datosOld[1];
		$localizador=$datosOld[2];
		echo "<tr>";
		echo "<td>$tabla</td>";
		echo "<td>$permiso</td>";
		echo "<td>$localizador</td>";
		echo "</tr>";
		}
		echo "</tbody></table></tr></td></table></br>";
	}	
	
}
}
}

}



function perLocOld($xmlold,$conexion,$nomFichpro){
	//Borramos temporales
	deltetbtmp("localizador",$conexion);
	deltetbtmp("consultas",$conexion);
	deltetbtmp("agrupacion",$conexion);
	deltetbtmp("agrupacion_tmp",$conexion);
	//Se quitan
	$nuPermLoc=count($xmlold->sqlComponents->sqlComponent);
	//echo $nuPermLoc;	
	if ($nuPermLoc>0){//Si tiene componentes sql
					
		//Insertamos todos los localizadores del xml nuevo
		foreach($xmlold->sqlComponents->sqlComponent as $sqlComponent) {
			$localizadores=$sqlComponent->parameters->sqlComponentParams;	//Tabla de localizadores
			$module=$localizadores->module;
			$component=$localizadores->component;
			$dataSourceAlias=$localizadores->dataSourceAlias;
				
			//Insertamos en la tabla localizador
			grabarLoc($conexion,$nomFichpro,$module,$component,$dataSourceAlias);
		
		
		
		
		//Insertamos en la tabla consultas
		foreach($sqlComponent->sqls->{'sql-sentence'} as $sentence) {
			$tablas=$sentence->sql;
			$tablas = strtoupper($tablas);
			$module=$sentence->module;
			$component=$sentence->component;
			//echo $tablas;
			//echo"<br/>";
			grabarComp ($conexion,$nomFichpro,$module,$component,$tablas,$i);
			$i++;
		}
		}
		
		insAgrupacionIni($conexion,$nomFichpro);
		separaTablas($conexion,$nomFichpro);
		
		//print "agrupPermisos";
		$consulta="SELECT distinct tabla FROM agrupacion_tmp";
		$cursor=mysqli_query($conexion,$consulta);
		if(!$cursor) {
		print "Error cursor agrupPermisos"; }
		else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = $datos[0];
			//print "tabla=$tabla";
			$localizador=obtLocTabla($conexion,$tabla);
			$permiso=obtPermTabla($conexion,$tabla);
			insAgrupacion ("agrup_perm_old",$conexion,$nomFichpro,$tabla,$permiso,$localizador);//Inserta el registro
			}
			
		}	

		
}
}

function perLocNew($xml,$conexion,$nomFichpro){
		
	//Se añade	
	//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Permisos Localizadores</b></font></td></tr></table>";	
	$nuPermLoc=count($xml->sqlComponents->sqlComponent);
	//echo $nuPermLoc;	
	if ($nuPermLoc>0){//Si tiene componentes sql
		
				
		//Insertamos todos los localizadores del xml nuevo
		foreach($xml->sqlComponents->sqlComponent as $sqlComponent) {
			$localizadores=$sqlComponent->parameters->sqlComponentParams;	//Tabla de localizadores
			$module=$localizadores->module;
			$component=$localizadores->component;
			$dataSourceAlias=$localizadores->dataSourceAlias;
				
			//Insertamos en la tabla localizador
			grabarLoc($conexion,$nomFichpro,$module,$component,$dataSourceAlias);
	
		
		//Insertamos en la tabla consultas
		foreach($sqlComponent->sqls->{'sql-sentence'} as $sentence) {
			$tablas=$sentence->sql;
			$tablas = strtoupper($tablas);
			$module=$sentence->module;
			$component=$sentence->component;
			//echo $tablas;
			//echo"<br/>";
			grabarComp ($conexion,$nomFichpro,$module,$component,$tablas,$i);
			$i++;
		}
		}
		
		insAgrupacionIni($conexion,$nomFichpro);
		separaTablas($conexion,$nomFichpro);
		
		//print "agrupPermisos";
		$consulta="SELECT distinct tabla FROM agrupacion_tmp";
		$cursor=mysqli_query($conexion,$consulta);
		if(!$cursor) {
		print "Error cursor agrupPermisos"; }
		else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = $datos[0];
			//print "tabla=$tabla";
			$localizador=obtLocTabla($conexion,$tabla);
			$permiso=obtPermTabla($conexion,$tabla);
			insAgrupacion ("agrup_perm_new",$conexion,$nomFichpro,$tabla,$permiso,$localizador);//Inserta el registro
			}
			
		}	
}
}

function grabarComp($conexion,$nombreFichero,$moduloNegocio, $componente, $tablas,$i) {
	
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$tablas=trim($tablas);
	
	//$tablas = strtoupper($tablas);
    $tipoConsulta = substr($tablas, 0, 1);

	//Select
	if ($tipoConsulta=='S') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
		if ($posWhere=="") {
		$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	//Insert
	if ($tipoConsulta=='I') {
		$permisos="INSERT";
		$posFrom = strpos($tablas, "INTO");
		$posWhere = strpos($tablas, "(");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("INTO", "", $tablasBBDD);
	}
	//Update
	if ($tipoConsulta=='U') {
		$permisos="UPDATE";
		$posFrom = strpos($tablas, "UPDATE");
		$posWhere = strpos($tablas, "SET");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("UPDATE", "", $tablasBBDD);
	}
	//Delete
	if ($tipoConsulta=='D') {
		$permisos="DELETE";
		$posFrom = strpos($tablas, "FROM");
		//print "*A*$posFrom**";
		$posFrom=$posFrom+4;
		//print "*D*$posFrom**";
		$posWhere = strpos($tablas, "WHERE");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	
	//With temp
	if ($tipoConsulta=='W') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strpos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	//echo "$tablasBBDD";
		
	//Si hay un segundo from
	$tablasBBDD2 = substr($tablas, $posWhere,strlen($tablas));
	$posFrom = strpos($tablasBBDD2, "FROM");
	if ($posFrom!=""){
	$posFrom=$posFrom+4;
	$posWhere = strpos($tablasBBDD2, "WHERE");
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD2 = substr($tablasBBDD2, $posFrom, $posWhere);
	}
	//echo "$tablasBBDD2";
	
	if ($tablasBBDD2!=""){
		$tablasBBDD=$tablasBBDD.",".$tablasBBDD2;}
	//$posAs=strpos($tablasBBDD, " AS");
	//$tablasBBDD = substr($tablasBBDD,0,$posAs);
	//echo "$tablasBBDD";
	//echo "</br>";
	}
	
	
	$tablasBBDD = str_replace("$", "", $tablasBBDD);
	$tablasBBDD = str_replace("SCHEMA.", "", $tablasBBDD);
	$tablasBBDD = str_replace("'", "", $tablasBBDD);
	if (trim($tablasBBDD)=="") {
			$tablasBBDD = $tablas;
	}
	
	//echo $tablasBBDD;
	//echo "</br>";
	//print "$i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos'";
	
	$consulta = "INSERT INTO consultas (fila,nombreFichero,modulo_negocio, componente, tablas,permiso) 
	VALUES ($i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos')";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	//echo "<br/>";
	if(mysqli_error($conexion)) {
	   //print "La siguiente consulta no ha podido ser procesada:";
	   //echo "</br>";
	   //echo "$consulta";
	   //echo "</br>";
	}
}

//Procesa las ttpp quitadas
function ttppold($conexion,$nomFichpro){
	$consulta="SELECT tabla,version,localizador FROM ttpp_old WHERE NOT EXISTS (SELECT * FROM ttpp_new WHERE ttpp_new.tabla=ttpp_old.tabla)";
	$cursor=mysqli_query($conexion,$consulta);
	
	if(!$cursor) {
	//print "alta";
	}else {
	//Se quitan	
		$n=mysqli_num_rows($cursor);
		//echo "n=$n";
		if ($n>0){
		echo "</br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Tablas de parámetros eliminadas</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		. "<thead>"
		. "<TR>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
		. "</TR>"
		. "</thead>"
		. "<tbody>";
		while($datos=mysqli_fetch_array($cursor)){
			$tabla=$datos[0];
			$version=$datos[1];
			$localizador=$datos[2];
			echo "<tr>";
			echo "<td>$tabla</td>";
			echo "<td>$version</td>";
			echo "<td>$localizador</td>";
			echo "</tr>";
			}	
		echo "</tbody></table></td></tr></table>";
		echo "</br>";
		}
	}
	

}

//Procesa las ttpp añadidas
function ttppnew($conexion,$nomFichpro){
	
	$consulta="SELECT tabla,version,localizador FROM ttpp_new WHERE NOT EXISTS (SELECT * FROM ttpp_old WHERE ttpp_old.tabla=ttpp_new.tabla)";
	$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	//print "alta";
	}else {
		//Se añaden	
		$n=mysqli_num_rows($cursor);
		if ($n>0){
		echo "</br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Tablas de parámetros añadidas</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		. "<thead>"
		. "<TR>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
		. "</TR>"
		. "</thead>"
		. "<tbody>";
		while($datos=mysqli_fetch_array($cursor)){
			$tabla=$datos[0];
			$version=$datos[1];
			$localizador=$datos[2];
			echo "<tr>";
			echo "<td>$tabla</td>";
			echo "<td>$version</td>";
			echo "<td>$localizador</td>";
			echo "</tr>";
			}	
		echo "</tbody></table></td></tr></table>";
		echo "</br>";
		}
	}
	
	
	
}

//Borra la tabla temporal de ttpp para poder reutilizarla
function deletetmpttpp($conexion){
deltetbtmp("ttpp",$conexion);
deltetbtmp("ttpp_tmp",$conexion);
}

//Tablas de parámetros
function tablasParametros($xml,$xmlold,$conexion,$nomFichpro,$chkMin,$chkNew){
		
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
	$global=$xml->global;
	$globalOld=$xmlold->global;
	
	//Comprueba si son iguales los xml 
	$result = xml_is_equal($global,$globalOld);
	if ($result === true) {
	// Si son iguales
	//echo"<td><font color = \"0B0B3B\"><b>Tablas de parametros</b></font> - Sin cambios</td>";
	} else {
		//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Tablas de parámetros</b></font></td></tr></table>";
		procTTPP($conexion,$nomFichpro,$xml,"ttpp_new");
		deletetmpttpp($conexion);
		procTTPP($conexion,$nomFichpro,$xmlold,"ttpp_old");
		//echo "chkNew:".$chkNew."chkMin:".$chkMin;
		echo "<table></table>";
		if ($chkNew) ttppnew($conexion,$nomFichpro);
		echo "<tr><td><table></table></tr></td>";
		if ($chkMin) ttppold($conexion,$nomFichpro);
	}
	
}

//Graba la ttpp
function grabarTTPP($conexion,$nomFichpro,$tdestino,$tabla,$version,$localizador) {
	//print "grabarTTPPIni";
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO $tdestino (TABLA,VERSION,LOCALIZADOR,nombreFichero) 
	VALUES ('$tabla', '$version', '$localizador','$nomFichpro')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	
	if(mysqli_error($conexion)) {
	  print "$consulta<br/>";
	  print "grabarTTPP-".$tdestino."-mysql_error<br/>";	
	}
}

	


//Graba la tabla inicial de localizadores
function grabarLoc($conexion,$nombreFichero,$moduloNegocio, $componente, $alias) {
	//print "Grabar";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO localizador (nombreFichero,modulo_negocio, componente, alias) 
	VALUES ('$nombreFichero','$moduloNegocio', '$componente', '$alias')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "grabarLoc-mysql_error";	
	}
}


//TrxOP Partenon
function trxOP($xml,$xmlold,$chkMin,$chkNew){
	//Partenon
	$trxOpPart=$xml->communication->trxOp;
	$trxOpPartOld=$xmlold->communication->trxOp;
	
	$result = xml_is_equal($trxOpPart,$trxOpPartOld);
	if ($result === true) {
	// Si son iguales
	//echo "</br>";//Para dejar espacio
	//echo"<td><font color = \"0B0B3B\"><b>TrxOP Partenon</b></font> - Sin cambios</td>";
	} else {
	
	//Titulo
	//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>TrxOP Partenon</b></font></td></tr></table>";		
	echo "<table></table>";	
	$protocolo=$xml->communication->trxOp->trxOpProtocol;
	$modo=$xml->communication->trxOp->trxOpDefaultMode;
	$redg=$xml->communication->trxOp->trxOpRedGProtocol;
	$tcpalias=$xml->communication->trxOp->trxOpTCPAlias;
	$protocoloold=$xml->communication->trxOp->trxOpProtocol;
	$modoold=$xml->communication->trxOp->trxOpDefaultMode;
	$redgold=$xml->communication->trxOp->trxOpRedGProtocol;
	$tcpaliasold=$xml->communication->trxOp->trxOpTCPAlias;
	
	if (xml_is_equal($protocolo,$protocoloold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Protocolo</b></font></td><td>".$protocolo."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Protocolo</b></font></td><td>".$protocolo."</b></font>";}
		
	if (xml_is_equal($protocolo,$protocoloold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Modo:</b></td><td>".$modo."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Modo:</b></td><td>".$modo."</b></font>";}
	
	if (xml_is_equal($protocolo,$protocoloold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>ProtocoloRedG:</b></td><td>".$redg."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>ProtocoloRedG:</b></td><td>".$redg."</b></font>";}
	
	if (xml_is_equal($protocolo,$protocoloold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>TCP Alias:</b></td><td>".$tcpalias."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>TCP Alias:</b></td><td>".$tcpalias."</b></font>";}
	
	//Se añade
	$numTrxOp=count($xml->communication->trxOp->trxOpStates->trxOpState);
	if ($numTrxOp>0&&$chkNew){//Si tiene trx
	//Cabecera tabla
		echo "<br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>TrxOP Partenon Añadidas o que cambian de versión:</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacción</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operación</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		echo"<br/>";
		
		//filas
		foreach($xml->communication->trxOp->trxOpStates->trxOpState as $trxOpState) {
		$igual=false;
		$aplicacion=$trxOpState->application;
		$oi=$trxOpState->internalOp;
		$trx=$trxOpState->transaction;
		$operacion=$trxOpState->operation;
		$version=$trxOpState->version;
			foreach($xmlold->communication->trxOp->trxOpStates->trxOpState as $trxOpStateOld) {
				//$aplicacionold=$trxOpStateOld->application;
				//$oiold=$trxOpStateOld->internalOp;
				$trxold=$trxOpStateOld->transaction;
				$operacionold=$trxOpStateOld->operation;
				$versionold=$trxOpStateOld->version;
				//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$operacion==$operacionold&&$version==$versionold){//Comparacion completa
				if (trim($trx)==trim($trxold)&&trim($operacion)==trim($operacionold)&&trim($version)==trim($versionold)){//Solo las trx
				$igual=true;
				}
			}
		if ($igual!=true){
		echo "<tr>";
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$operacion</td>";
		echo "<td>$version</td>";
		echo "</tr>";}
		}
		echo "</tbody></table></td></tr></table><br/>";
				
	}
	
	echo "<table></table>";
	
	//Se quita
	$numTrxOpOld=count($xmlold->communication->trxOp->trxOpStates->trxOpState);
	if ($numTrxOpOld>0&&$chkMin){//Si tiene trx
	//Cabecera tabla
		echo "<br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>TrxOP Partenon Eliminadas:</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacción</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operación</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		echo"<br/>";
		
		//filas
		foreach($xmlold->communication->trxOp->trxOpStates->trxOpState as $trxOpStateOld) {
		$igual=false;
		$aplicacionold=$trxOpStateOld->application;
		$oiold=$trxOpStateOld->internalOp;
		$trxold=$trxOpStateOld->transaction;
		$operacionold=$trxOpStateOld->operation;
		$versionold=$trxOpStateOld->version;
			foreach($xml->communication->trxOp->trxOpStates->trxOpState as $trxOpState) {
				//$aplicacion=$trxOpState->application;
				//$oi=$trxOpState->internalOp;
				$trx=$trxOpState->transaction;
				$operacion=$trxOpState->operation;
				$version=$trxOpState->version;
				//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$operacion==$operacionold&&$version==$versionold){//Comparacion completa
				if (trim($trx)==trim($trxold)&&trim($operacion)==trim($operacionold)&&trim($version)==trim($versionold)){//Solo las trx
				$igual=true;
				}
			}
		if ($igual!=true){
		echo "<tr>";
		echo "<td>$aplicacionold</td>";
		echo "<td>$oiold</td>";
		echo "<td>$trxold</td>";
		echo "<td>$operacionold</td>";
		echo "<td>$versionold</td>";
		echo "</tr>";}
		}
		echo "</tbody></table></td></tr></table><br/>";
				
	}
	
	
	}
	
	
	//Sat
	$sat=$xml->communication->sat;
	$satOld=$xmlold->communication->sat;
	//Comparamos xmls
	$result = xml_is_equal($sat,$satOld);
	if ($result === true) {
	// Si son iguales
	//echo "</br>";//Para dejar espacio
	//echo"<td><font color = \"0B0B3B\"><b>Sat</b></font> - Sin cambios</td>";
	} else {
	//echo"<br/>";
	//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Sat</b></font></td></tr></table>";
		
	$protsat=$xml->communication->sat->satProtocol;
	$aliassat=$xml->communication->sat->satAlias;
	$canallog=$xml->communication->sat->satLogicalChannels;
	$canalfis=$xml->communication->sat->satPyshicalChannels;
	$protsatold=$xmlold->communication->sat->satProtocol;
	$aliassatold=$xmlold->communication->sat->satAlias;
	$canallogold=$xmlold->communication->sat->satLogicalChannels;
	$canalfisold=$xmlold->communication->sat->satPyshicalChannels;
	
	if (xml_is_equal($protsat,$protsatold)==false) {
		//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Protocolo</b></font></td><td>".$protsat."</td></tr></tr></table>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Protocolo</b></font>".$protsat; }
		
	if (xml_is_equal($aliassat,$aliassatold)==false) {
		//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Alias:</b></td><td>".$aliassat."</td></tr></tr></table>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Alias:</b></font>".$aliassat; }
	
	if (xml_is_equal($canallog,$canallogold)==false) {
		//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Canal Lógico:</b></td><td>".$canallog."</td></tr></tr></table>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Canal Lógico:</b></font>".$canallog; }
	
	if (xml_is_equal($canalfis,$canalfisold)==false) {
		//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Canal Físico:</b></td><td>".$canalfis."</td></tr></tr></table>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Canal Físico:</b></font>".$canalfis; }
	
	echo "<table></table>";
	
	//Se añade
	$kids = $xml->communication->sat->satStates->children();
	$numSat=count($kids);
	//echo $numSat;
	
	if ($numSat>0&&$chkNew){//Si tiene trx	
	
		//Cabecera tabla
		echo"</br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Sat Añadidas</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Nombre</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
		//filas
		foreach($xml->communication->sat->satStates->satState as $satState) {
		$igual=false;
		$aplicacion=$satState->application;
		$oi=$satState->internalOp;
		$trx=$satState->satName;
		$version=$satState->version;
			foreach($xmlold->communication->sat->satStates->satState as $satStateOld) {
			//$aplicacionold=$satStateOld->application;
			//$oiold=$satStateOld->internalOp;
			$trxold=$satStateOld->satName;
			$versionold=$satStateOld->version;
			//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$version==$versionold){
			if (trim($trx)==trim($trxold)&&trim($version)==trim($versionold)){
				$igual=true;
			}
			}
			if ($igual!=true&&$trx!=""){
				echo "<td>$aplicacion</td>";
				echo "<td>$oi</td>";
				echo "<td>$trx</td>";
				echo "<td>$version</td>";
			echo "</tr>";}
		}	
		echo "</tbody></table></td></tr></table>";
	}
	
	echo "<table></table>";
	
	//Se quita
	$kidsold = $xmlold->communication->sat->satStates->children();
	$numSatold=count($kidsold);
	//echo $numSat;
	
	if ($numSatold>0&&$chkMin){//Si tiene SAT
	
		//Cabecera tabla
		echo"</br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\"><b>Sat Eliminadas</b></font>";
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Nombre</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		echo"<br/>";
		
		//filas
		foreach($xmlold->communication->sat->satStates->satState as $satStateold) {
		$igual=false;
		$aplicacionold=$satStateold->application;
		$oiold=$satStateold->internalOp;
		$trxold=$satStateold->satName;
		$versionold=$satStateold->version;
			foreach($xml->communication->sat->satStates->satState as $satState) {
			$aplicacion=$satState->application;
			$oi=$satState->internalOp;
			$trx=$satState->satName;
			$version=$satState->version;
			//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$version==$versionold){
			if (trim($trx)==trim($trxold)&&trim($version)==trim($versionold)){
				$igual=true;
			}
			}
		if ($igual!=true&&$trxold!=""){
		
		echo "<td>$aplicacionold</td>";
		echo "<td>$oiold</td>";
		echo "<td>$trxold</td>";
		echo "<td>$versionold</td>";
		echo "</tr>";}
		

		}	
		echo "</tbody></table></td></tr></table><br/>";
	}
	
	}
	
	//Altair
	$altair=$xml->communication->altair;
	$altairOld=$xmlold->communication->altair;
	//Comparamos xmls
	$result = xml_is_equal($altair,$altairOld);
	if ($result === true) {
	// Si son iguales
	//echo "</br>";//Para dejar espacio
	//echo"<td><font color = \"0B0B3B\"><b>TrxOP Altair</b></font> - Sin cambios</td>";
	} else {
	
	//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>TrxOP Altair</b></font></td></tr></table>";
	echo "<table></table>";
	$protaltair=$xml->communication->altair->altairProtocol;
	$altairalias=$xml->communication->altair->altairAlias;
	$protaltairold=$xmlold->communication->altair->altairProtocol;
	$altairaliasold=$xmlold->communication->altair->altairAlias;
	
	if (xml_is_equal($protaltair,$protaltairold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Protocolo</b></font></td><td>".$protaltair."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Protocolo:</b></font>$protaltair";}
		
	if (xml_is_equal($altairalias,$altairaliasold)==false) {
	//echo"<table border=\"1\"><tr><td bgcolor=\"#EEECDC\"><b>Modo:</b></td><td>".$altairalias."</td></tr></tr></table>";
	echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Modo:</b></font>$altairalias";}
	
	//Se añade
	$kids = $xml->communication->altair->altairStates->children();
	$numTrxAltair=count($kids);
	
	if ($numTrxAltair>0&&$chkNew){//Si tiene trx
	
	//Cabecera tabla
		echo"</br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Transacciones Altair Añadidas</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacción</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		
		
		//filas
		foreach($xml->communication->altair->altairStates->altairState as $altairState) {
		$igual=false;
		$aplicacion=$altairState->application;
		$oi=$altairState->internalOp;
		$trx=$altairState->transaction;
		$version=$altairState->version;
			foreach($xmlold->communication->altair->altairStates->altairState as $altairStateOld) {
			$aplicacionold=$altairStateOld->application;
			$oiold=$altairStateOld->internalOp;
			$trxold=$altairStateOld->transaction;
			$versionold=$altairStateOld->version;
				//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$version==$versionold){
				if (trim($trx)==trim($trxold)&&trim($version)==trim($versionold)){
				$igual=true;
				}
			}
		if ($igual!=true){
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$version</td>";
		echo "</tr>";}
		
		}
		echo "</tbody></table></td></tr></table><br/>";
				
	}
	
	echo "<table></table>";
	
	//Se quita
	$kidsold = $xmlold->communication->altair->altairStates->children();
	$numTrxAltairold=count($kidsold);
	
	if ($numTrxAltairold>0&&$chkMin){//Si tiene trx
	
	//Cabecera tabla
		echo "<br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Transacciones Altair Eliminadas</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacción</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versión</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		
		
		//filas
		foreach($xmlold->communication->altair->altairStates->altairState as $altairStateOld) {
		$igual=false;
		$aplicacion=$altairStateOld->application;
		$oi=$altairStateOld->internalOp;
		$trx=$altairStateOld->transaction;
		$version=$altairStateOld->version;
			foreach($xml->communication->altair->altairStates->altairState as $altairState) {
			$aplicacionold=$altairState->application;
			$oiold=$altairState->internalOp;
			$trxold=$altairState->transaction;
			$versionold=$altairState->version;
				//if ($aplicacion==$aplicacionold&&$oi==$oiold&&$$trx==$trxold&&$version==$versionold){
				if (trim($trx)==trim($trxold)&&trim($version)==trim($versionold)){
				$igual=true;
				}
			}
		if ($igual!=true){
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$version</td>";
		echo "</tr>";}
		
		}
		echo "</tbody></table></td></tr></table><br/>";
				
	}
	
	}
}



//WS Expuestos
function wsExpuestos($xml,$xmlold,$chkMin,$chkNew){

$wsExpNew=$xml->channelAdapters;
$wsExpOld=$xmlold->channelAdapters;
	
$result = xml_is_equal($wsExpNew,$wsExpOld);
if ($result === true) {
   // Si son iguales
   //echo"<td><font color = \"0B0B3B\"><b>Web Services Expuestos</b></font> - Sin cambios</td>";
} else {
   	
   //echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Expuestos</b></font></td></tr></table>";
	$nuWsExp=count($xml->channelAdapters->channelAdapter);
		
		
	//Se añaden
	if ($nuWsExp>0&&$chkNew){//Si tiene ws
		//Cabecera tabla
		echo "<br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Web Services Expuestos Añadidos</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Adaptador</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Fachada</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
		//filas
		foreach($xml->channelAdapters->channelAdapter as $channelAdapter) {
			$igual=false;
			$adaptador=trim($channelAdapter->adapterName);
			$tipo=trim($channelAdapter->type);
			$fachada=trim($channelAdapter->facadeName);
			$alias=trim($channelAdapter->alias);
			foreach($xmlold->channelAdapters->channelAdapter as $channelAdapterold) {
				$adaptadorold=trim($channelAdapterold->adapterName);
				$tipoold=trim($channelAdapterold->type);
				$fachadaold=trim($channelAdapterold->facadeName);
				$aliasold=trim($channelAdapterold->alias);
				if ($adaptador==$adaptadorold&&$tipo==$tipoold&&$fachada==$fachadaold&&$alias==$aliasold){
				$igual=true;
				}
			}
		if ($igual==true){
		}else{
		echo "<tr>";
		echo "<td>$adaptador</td>";
		echo "<td>$tipo</td>";
		echo "<td>$fachada</td>";
		echo "<td>$alias</td>";
		echo "</tr>";}
		}
		
		echo "</tbody></table></tr></td></table>";
		}
		
		echo "<table></table>";
		
		//Se quitan
		$nuWsExpOld=count($xmlold->channelAdapters->channelAdapter);
		if ($nuWsExpOld>0&&$chkMin){//Si tiene ws
		//Cabecera tabla
		echo "<br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Web Services Expuestos Eliminados</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Adaptador</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Fachada</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
		//filas
		foreach($xmlold->channelAdapters->channelAdapter as $channelAdapterold) {
			$igual=false;
			$adaptadorold=trim($channelAdapterold->adapterName);
			$tipoold=trim($channelAdapterold->type);
			$fachadaold=trim($channelAdapterold->facadeName);
			$aliasold=trim($channelAdapterold->alias);
			foreach($xml->channelAdapters->channelAdapter as $channelAdapter) {
				$adaptador=trim($channelAdapter->adapterName);
				$tipo=trim($channelAdapter->type);
				$fachada=trim($channelAdapter->facadeName);
				$alias=trim($channelAdapter->alias);
				if ($adaptador==$adaptadorold&&$tipo==$tipoold&&$fachada==$fachadaold&&$alias==$aliasold){
				$igual=true;
				}
			}
		if ($igual==true){
		}else{
		echo "<tr>";
		echo "<td>$adaptadorold</td>";
		echo "<td>$tipoold</td>";
		echo "<td>$fachadaold</td>";
		echo "<td>$aliasold</td>";
		echo "</tr>";}
		}
		echo "</tbody></table></tr></td></table>";
		
		}

	
	}
		
}

//WS Consumidos
function wsConsumidos($xml,$xmlold,$chkMin,$chkNew){

$wsExpNew=$xml->webServices;
$wsExpOld=$xmlold->webServices;
	
$result = xml_is_equal($wsExpNew,$wsExpOld);
if ($result === true) {
   // Si son iguales
   //echo "</br>";//Para dejar espacio
   //echo"<td><font color = \"0B0B3B\"><b>Web Services Consumidos</b></font> - Sin cambios</td>";
} else {
 
	//echo "</br>";//Para dejar espacio
	//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Consumidos</b></font></td></tr></table>";
	
	//Se añaden	
	$nuWsCon=count($xml->webServices->webServiceStates->webServiceState);
		
	if ($nuWsCon>0&&$chkNew){//Si tiene ws
		//Cabecera tabla
		echo "<br>";
		echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>Web Services Consumidos Añadidos</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
			//filas se añaden
			foreach($xml->webServices->webServiceStates->webServiceState as $wsstates) {
			$igual==false;
			$aplicacion=$wsstates->application;
			$operacion=$wsstates->internalOP;
			$alias=$wsstates->alias;
			$tipo=$wsstates->transport;
			foreach($xmlold->webServices->webServiceStates->webServiceState as $wsstatesold) {
				$aplicacionold=$wsstatesold->application;
				$operacionold=$wsstatesold->internalOP;
				$aliasold=$wsstatesold->alias;
				$tipoold=$wsstatesold->transport;
				if ($aplicacion==$aplicacionold&&$operacion==$operacionold&&$alias==$aliasold&&$tipo==$tipoold){
				$igual=true;
				}
			}
			if ($igual==true){
			}else{
			echo "<tr>";
			echo "<td>$aplicacion</td>";
			echo "<td>$operacion</td>";
			echo "<td>$alias</td>";
			echo "<td>$tipo</td>";
			echo "</tr>";}
		}
		echo "</tbody></table></tr></td></table>";
		}
		
		//Se quitan
		$nuWsConOld=count($xmlold->webServices->webServiceStates->webServiceState);
		
		if ($nuWsConOld>0&&$chkMin){//Si tiene ws
		//Cabecera tabla
		echo "<br>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>Web Services Consumidos Eliminados</b></font>"; 
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operación</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
			//filas se añaden
			foreach($xmlold->webServices->webServiceStates->webServiceState as $wsstatesold) {
			$igual==false;
			$aplicacion=$wsstatesold->application;
			$operacion=$wsstatesold->internalOP;
			$alias=$wsstatesold->alias;
			$tipo=$wsstatesold->transport;
			foreach($xml->webServices->webServiceStates->webServiceState as $wsstates) {
				$aplicacionold=$wsstates->application;
				$operacionold=$wsstates->internalOP;
				$aliasold=$wsstates->alias;
				$tipoold=$wsstates->transport;
				if ($aplicacion==$aplicacionold&&$operacion==$operacionold&&$alias==$aliasold&&$tipo==$tipoold){
				$igual=true;
				}
			}
			if ($igual==true){
			}else{
			echo "<tr>";
			echo "<td>$aplicacion</td>";
			echo "<td>$operacion</td>";
			echo "<td>$alias</td>";
			echo "<td>$tipo</td>";
			echo "</tr>";}
		
		
		}
		echo "</tbody></table></tr></td></table>";
		}
}	
	echo "</br>";//Para dejar espacio	
}

function xml_is_equal(SimpleXMLElement $xml1, SimpleXMLElement $xml2, $text_strict = false) {
	// compare text content
	if ($text_strict) {
		if ("$xml1" != "$xml2") return "mismatched text content (strict)";
	} else {
		if (trim("$xml1") != trim("$xml2")) return "mismatched text content";
	}

	// check all attributes
	$search1 = array();
	$search2 = array();
	foreach ($xml1->attributes() as $a => $b) {
		$search1[$a] = "$b";		// force string conversion
	}
	foreach ($xml2->attributes() as $a => $b) {
		$search2[$a] = "$b";
	}
	if ($search1 != $search2) return "mismatched attributes";
	
	// check all namespaces
	$ns1 = array();
	$ns2 = array();
	foreach ($xml1->getNamespaces() as $a => $b) {
		$ns1[$a] = "$b";
	}
	foreach ($xml2->getNamespaces() as $a => $b) {
		$ns2[$a] = "$b";
	}
	if ($ns1 != $ns2) return "mismatched namespaces";
	
	// get all namespace attributes
	foreach ($ns1 as $ns) {			// don't need to cycle over ns2, since its identical to ns1
		$search1 = array();
		$search2 = array();
		foreach ($xml1->attributes($ns) as $a => $b) {
			$search1[$a] = "$b";
		}
		foreach ($xml2->attributes($ns) as $a => $b) {
			$search2[$a] = "$b";
		}
		if ($search1 != $search2) return "mismatched ns:$ns attributes";
	}
	
	// get all children
	$search1 = array();
	$search2 = array();
	foreach ($xml1->children() as $b) {
		if (!isset($search1[$b->getName()]))
			$search1[$b->getName()] = array();
		$search1[$b->getName()][] = $b;
	}
	foreach ($xml2->children() as $b) {
		if (!isset($search2[$b->getName()]))
			$search2[$b->getName()] = array();
		$search2[$b->getName()][] = $b;
	}
	// cycle over children
	if (count($search1) != count($search2)) return "mismatched children count";		// xml2 has less or more children names (we don't have to search through xml2's children too)
	foreach ($search1 as $child_name => $children) {
		if (!isset($search2[$child_name])) return "xml2 does not have child $child_name";		// xml2 has none of this child
		if (count($search1[$child_name]) != count($search2[$child_name])) return "mismatched $child_name children count";		// xml2 has less or more children
		foreach ($children as $child) {
			// do any of search2 children match?
			$found_match = false;
			$reasons = array();
			foreach ($search2[$child_name] as $id => $second_child) {
				if (($r = xml_is_equal($child, $second_child)) === true) {
					// found a match: delete second
					$found_match = true;
					unset($search2[$child_name][$id]);
				} else {
					$reasons[] = $r;
				}
			}
			if (!$found_match) return "xml2 does not have specific $child_name child: " . implode("; ", $reasons);
		}
	}
	
	// finally, cycle over namespaced children 
	foreach ($ns1 as $ns) {			// don't need to cycle over ns2, since its identical to ns1
		// get all children
		$search1 = array();
		$search2 = array();
		foreach ($xml1->children() as $b) {
			if (!isset($search1[$b->getName()]))
				$search1[$b->getName()] = array();
			$search1[$b->getName()][] = $b;
		}
		foreach ($xml2->children() as $b) {
			if (!isset($search2[$b->getName()]))
				$search2[$b->getName()] = array();
			$search2[$b->getName()][] = $b;
		}
		// cycle over children
		if (count($search1) != count($search2)) return "mismatched ns:$ns children count";		// xml2 has less or more children names (we don't have to search through xml2's children too)
		foreach ($search1 as $child_name => $children) {
			if (!isset($search2[$child_name])) return "xml2 does not have ns:$ns child $child_name";		// xml2 has none of this child
			if (count($search1[$child_name]) != count($search2[$child_name])) return "mismatched ns:$ns $child_name children count";		// xml2 has less or more children
			foreach ($children as $child) {
				// do any of search2 children match?
				$found_match = false;
				foreach ($search2[$child_name] as $id => $second_child) {
					if (xml_is_equal($child, $second_child) === true) {
						// found a match: delete second
						$found_match = true;
						unset($search2[$child_name][$id]);
					}
				}
				if (!$found_match) return "xml2 does not have specific ns:$ns $child_name child";
			}
		}
	}	
	
	// if we've got through all of THIS, then we can say that xml1 has the same attributes and children as xml2.
	return true;
}

function catalogoContenidos($xml,$xmlold){
	$catcon=$xml->other->catCon;
	$catconold=$xmlold->other->catCon;
	$catcon=trim($catcon);
	$catconold=trim($catconold);
	if ($catcon=="yes") $catcon="si";
	if ($catconold=="yes") $catcon="si";
	//echo "antes:".$catcon.".";
	//echo "ahora:".$catconold.".";
	if ($catcon!=$catconold){
		//echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Catalogo de Contenidos</b></font></td></tr></table>";
		echo "<font color = \"4F6E92\" SIZE=3 ALIGN = \"center\" ><b>El Catalogo de contenidos ha cambiado: Antes ".$catconold." y ahora ".$catcon."</b></font>";
		//echo"<table><tr><td>El Catalogo de contenidos ha cambiado: Antes ".$catconold." y ahora ".$catcon."</td></tr></table>";
	}
	//else{
		//echo "</br>";//Para dejar espacio
		//echo"<td><font color = \"0B0B3B\"><b>Catalogo de Contenidos</b></font> - Sin cambios</td>";
		//}
}

function procTTPP($conexion,$nombreFichero,$xml,$tdestino){

foreach($xml->global->treeTbls->treeTbl as $cacheadas) {
			$tabla=$cacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla cacheada:".$tabla;
			grabarTTPP($conexion,$nomFichpro,'ttpp_tmp',$tabla,'','');
			}
			
	//no cacheadas
foreach($xml->global->mgrTables->mgrTable as $nocacheadas) {
			$tabla=$nocacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla no cacheada:".$tabla;
			grabarTTPP($conexion,$nomFichpro,'ttpp_tmp',$tabla,'','');
			
			}

//print "procTTPP";
$consulta =  "SELECT DISTINCT TABLA FROM ttpp_tmp order by TABLA";
$cursor=mysqli_query($conexion,$consulta);
if(!$cursor) {
	//print "alta";
}
else {
		while($datos=mysqli_fetch_array($cursor)){
			$tabla = $datos[0];
			//print "tabla=$tabla";
			if ($tabla!='root' and $tabla!='rootAux'){//Quitamos morralla de sgs
				$version=obtVerTTPP($conexion,'ttpp_uk',$tabla,'','');
				$localizador=obtLocTTPP($conexion,'ttpp_uk',$tabla,'','');
				grabarTTPP2($conexion,$nombreFichero,$tdestino,$tabla,$version,$localizador);
				//echo "</br>";
				//print "tabla=$tabla";
			}
		}		
	}
}



//Graba la ttpp
function grabarTTPP2($conexion,$nombreFichero,$tdestino,$tabla,$version, $localizador) {
	//print "Grabar";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO $tdestino (TABLA,VERSION,LOCALIZADOR,nombreFichero) 
	VALUES ('$tabla', '$version', '$localizador','$nombreFichero')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	
	if(mysqli_error($conexion)) {
	  print "$consulta<br/>";
	  print "grabarTTPP-".$tdestino."-mysql_error<br/>";	
	   
	}
}

//Obtiene la versión la ttpp
function obtVerTTPP($conexion,$tdestino,$tabla, $version, $localizador){
//print "obtVerTTPP";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "Select VERSION from $tdestino where TABLA='$tabla'";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	while($datos=mysqli_fetch_array($cursor)){
	return $datos[0];}
	//print "$consulta";
	if(mysql_error()) {
	   print "obtVerTTPP-mysql_error";	
	}
}

//Obtiene el localizador de la ttpp
function obtLocTTPP($conexion,$tdestino,$tabla, $version, $localizador){
//print "obtVerTTPP";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "Select LOCALIZADOR from $tdestino where TABLA='$tabla'";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	while($datos=mysqli_fetch_array($cursor)){
	return $datos[0];}
	//print "$consulta";
	if(mysql_error()) {
	   print "obtLocTTPP-mysql_error";	
	}
}

function obtnomfich($conexion) {
$consulta="SELECT distinct nombreFichero FROM consultas";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor agrupPermisos"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
		return $datos[0];
		//echo "Nombre del fichero: $nomFichpro";
		}}
}


//Inicializa la tabla de agrupaciones
function insAgrupacionIni($conexion,$nomFichpro) {
//print "insAgrupacionIni";
$consulta =  "SELECT fila,tablas,permiso,alias,b.nombreFichero FROM consultas a,localizador b where a.modulo_negocio =b.modulo_negocio and a.componente = b.componente and a.nombreFichero=b.nombreFichero 
and b.nombreFichero='$nomFichpro' order by tablas";
//print $consulta;	
$cursor=mysqli_query($conexion,$consulta);
if(!$cursor) {
	print "alta";
}
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$numero = $datos[0];
			$tablas=trim($datos[1]);
			$permiso=trim($datos[2]);
			$localizador=trim($datos[3]);
			$nomFichpro=trim($datos[4]);
			//print "tablas:$tablas**";
			//echo "</br>";
			if (strpos($tablas,'UNION SELECT') !== false) { //Tratamos los UNION SELECT
				$tablas=tratarJoins ('UNION SELECT',$tablas);
			}
			if (strpos($tablas,'SELECT') !== false) { //Tratamos los SUB-SELECT
				$tablas=trataSubSelect($tablas);
			}
			if (strpos($tablas,'INNER JOIN') !== false) { //Tratamos los INNER JOIN
				$tablas=tratarJoins ('INNER JOIN',$tablas);
			}
			if (strpos($tablas,'LEFT JOIN') !== false) {//Tratamos los LEFT JOIN
				$tablas=tratarJoins ('LEFT JOIN',$tablas);
			}
			if (strpos($tablas,'RIGHT JOIN') !== false) {//Tratamos los RIGHT JOIN
				$tablas=tratarJoins ('RIGHT JOIN',$tablas);
			}
			
			$tablas=trim($tablas);
			//Print "tabla=$tablas**";
			//echo "</br>";
			insAgrupacion ("agrupacion",$conexion,$nomFichpro,$tablas, $permiso, $localizador); 
		}	
	}
	

}

//Separamos las tablas en las celdas
function separaTablas($conexion,$nomFichpro)
{
//print "separaTablas";
$consulta="SELECT tabla,permiso,nombreFichero,localizador FROM agrupacion group by tabla,permiso";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor vseparaTablas"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = trim($datos[0]);
			$permiso=$datos[1];
			$nomFichpro=$datos[2];
			$localizador=$datos[3];
			$elements = explode(",", $tabla);
				for ($ii = 0; $ii < count($elements); $ii++) {
				$nquotes = trim($elements[$ii]);//Elimina espacios al comienzo y al final
				if (strpos($nquotes," AS ") !== false) {//Si tiene clausula as
				$nquotes=strstr($nquotes ," AS ",true);}//Eliminamos la clausula as
				if (strpos($nquotes," ")!== false) {//Si tiene seudonimo
				$nquotes=strstr($nquotes," ",true);}//Eliminamos seudonimo
				$nquotes=trim($nquotes);
				if ($nquotes!=""){
				insAgrupacion ("agrupacion_tmp",$conexion,$nomFichpro,$nquotes,$permiso,$localizador);}
				}
			
		}	
	}
	
}

//Pinta los datos de permisos de localizadores
function pintarDatos($conexion,$nomFichpro) {
//print "pintar";
$consulta =  "SELECT tabla,permiso,localizador FROM agrup_perm WHERE nombreFichero='$nomFichpro' order by tabla";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
		//print "alta";
	}
	else {
	    
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla=$datos[0];
			$permiso=$datos[1];
			$localizador=$datos[2];
			echo "<tr>";
			echo "<td>$tabla</td>";
			echo "<td>$permiso</td>";
			echo "<td>$localizador</td>";
			echo "</tr>";
		}	
		echo "</tbody></table></tr></td></table></br>";
	}
	
	echo "</br>";
	echo"<table><tr><td></td></tr></table></br>";	
	
}

function tratarJoins ($tipo,$tablas){

	//Extraemos tabla 1
	$tbl1=strstr($tablas ,"$tipo",true);
	if (strpos($tbl1,' ON ') !== false){//Comprobamos si tiene clausula ON
			$tbl1=strstr($tbl1," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl1,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tbl1=strstr($tbl1,"WHERE",true);//Extraemos solo la tabla
	}
	$tbl1=str_replace("$tipo","",$tbl1);//Eliminamos clausula
	//print "tabla 1-$tipo:$tbl1";
	
	
	//Extraemos tabla 2			
	$tbl2=strstr($tablas ,"$tipo",false);
	if (strpos($tbl2,' ON ') !== false){//Comprobamos si tiene clausula ON
		$tbl2=strstr($tbl2," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl2,'FROM') !== false){//Comprobamos si tiene clausula FROM (UNION)
			$tbl2=strstr($tbl2,"FROM",false);//Extraemos solo la tabla
			$tbl2=str_replace("FROM","",$tbl2);//Eliminamos clausula
	}
	$tbl2=str_replace("$tipo","",$tbl2);//Eliminamos clausula

	//print "tabla 2-$tipo:$tbl2";
	//echo "<br/>";
				
	$tablas=trim($tbl1).",".trim($tbl2);//Quitamos espacios y unimos por coma
	//echo "$tablas";
	return $tablas;
}

function trataSubSelect ($tablas){

	if (strpos($tablas,'FROM') !== false){//Comprobamos si tiene clausula FROM 
			$tablas=strstr($tablas,"FROM",false);//Extraemos solo la tabla
			$tablas=str_replace("FROM","",$tablas);//Eliminamos clausula
	}
	if (strpos($tablas,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tablas=strstr($tablas,"WHERE",true);//Extraemos solo la tabla
	}
	//print "trataSubSelect: $tablas";
	return $tablas;
}


//Inserta registros en las tablas de agrupaciones
function insAgrupacion ($tdestino,$conexion,$nomFichpro,$tabla, $permiso, $localizador) {
	//print "insAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "INSERT INTO $tdestino (tabla,nombreFichero, permiso, localizador) 
	VALUES ('$tabla', '$nomFichpro','$permiso', '$localizador')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysql_error" ;
	}
}


//Borra registros en la tabla agrupación
function delAgrupacion ($conexion,$nomFichpro,$tabla,$permiso) {
	//print "delAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "DELETE FROM agrupacion where tabla='$tabla' and permiso='$permiso'";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysql_error" ;
	}
}

//Obtiene los localizadores de cada tabla
function obtLocTabla($conexion,$tabla){
//print "obtLocTabla";
$consulta="SELECT DISTINCT localizador FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtLocTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$localizador=$datos[0];
		}
	}
	return $localizador;
}
	
//Obtiene los permisos de cada tabla
function obtPermTabla($conexion,$tabla)
{
//print "obtPermTabla";
$consulta="SELECT DISTINCT permiso FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtPermTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			if ($permiso!=""){
			$permiso=$permiso.",".$datos[0];}
			else{
			$permiso=$datos[0];}
		}
	}
	return $permiso;
}


?>
