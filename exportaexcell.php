﻿<?php 
/** Incluir la libreria PHPExcel */
//require_once '/Classes/PHPExcel.php';
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE); 
ini_set('display_startup_errors', TRUE); 
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

//Recogemos los valores del formulario
$producto=$_REQUEST['producto'];
$publicacion=$_REQUEST['publicacion'];
$rfc=$_REQUEST['rfc'];
$entrega=$_REQUEST['entrega'];
$rijel=$_REQUEST['rijel'];
$usabilidad=$_REQUEST['usabilidad'];
$arq=$_REQUEST['arq'];
$prop=$_REQUEST['prop'];

//Conectamos con la bbdd
$_servidor="localhost";
$_usuario="root";
$_clave="";
$conexion=mysqli_connect($_servidor,$_usuario,$_clave,"isban");

//Cargamos fichero
$nombreFichero = $_SESSION['fichero']; //Nombre del fichero xml
//echo $nombreFichero;
$excel95=$_SESSION['excel95'];//Check de excel95
$excel07=$_SESSION['excel07'];//Check de excel07



$xml = simplexml_load_file("tmp/".$nombreFichero);
$nomens=$xml->assembly['name'];
error_reporting(E_ALL ^ E_NOTICE);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Set document properties" , PHP_EOL;
$Titulo="Analisis_Tecnico_".$nomens."_" . date("d/m/Y") . "<br>";
$objPHPExcel->getProperties()->setCreator("System Configuration")
							 ->setLastModifiedBy("System Configuration")
							 ->setTitle($Titulo)
							 ->setSubject($publicacion)
							 ->setDescription("Analisis técnico de la aplicación ".$nomens)
							 ->setKeywords("analisis tecnico ens bks")
							 ->setCategory("Analisis");


							 
//Creamos el formato de la columna en un array
$coltitulo=array(
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),	
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '305496')
            ),
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 10,
			'name'  => 'Calibri'
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);


//Creamos el formato de la columna en un array
$coltabla=array(
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				),
			),	
         	'font'  => array(
			'size'  => 9,
			'name'  => 'Calibri'
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);

		
//Inventario numero de componentes
numcomp($conexion,$xml,$producto,$publicacion);

//Datos Generales
datosGenerales($objPHPExcel,$xml,$producto,$publicacion,$entrega,$rfc,$rijel,$usabilidad,$arq,$prop);

//Pintar pestañas rfc ssmm y rfc host
pestrfc($objPHPExcel);

//Pintar pestaña web services expuestos
wsExpuestos($objPHPExcel,$coltitulo,$coltabla,$xml);

//Pintar pestaña web services consumidos
wsConsumidos($objPHPExcel,$coltitulo,$coltabla,$xml);

//Pintar pestaña Tablas de parámetros
tablasParametros($objPHPExcel,$coltitulo,$coltabla,$xml,$conexion,$nombreFichero,$publicacion);

//Pintar pestaña trxOP
trxOP($objPHPExcel,$coltitulo,$coltabla,$xml,$publicacion);

//Pintar pestaña de localizadores
perLocalizadores($objPHPExcel,$coltitulo,$coltabla,$xml,$conexion,$doc,$nombreFichero,$publicacion);

//Pintar pestaña catálogo de contenidos
catalogoContenidos($objPHPExcel,$xml);

//Pintar numero de componentes
pintarNumComp($objPHPExcel,$consulta,$conexion,$publicacion);

//Borrar tablas temporales
deletetemp($conexion,$nombreFichero);

//Seleccionamos como pestaña activa la de datos generales
$objPHPExcel->setActiveSheetIndex(0);

//echo "excel95:".$excel95;
//echo "excel07:".$excel07;

// Guardar en formato Excel2007
if ($excel07){

//echo "</br>";
echo date('H:i:s') , " Escribe en formato Excel2007";
$callStartTime = microtime(true);
echo "</br>";

$nomfichero2007="Analisis_tecnico_".$nomens.".xlsx";//Nombre del documento
$carpeta=$xml->assembly['defaultBankChannel'];
//echo __DIR__ ."/Analisis Tecnicos/".$carpeta;
if (!file_exists(__DIR__ ."/Analisis_Tecnicos/".$carpeta)) {
mkdir(__DIR__ ."/Analisis_Tecnicos/".$carpeta, 0700);}
$pathfichero2007="/Analisis_Tecnicos/".$carpeta."/".$nomfichero2007;//Ruta para guardar el documento

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('exportaexcell.php',$pathfichero2007, __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

//echo date('H:i:s') , " Fichero guardado " , str_replace('exportaexcell.php',$nomfichero2007, pathinfo(__FILE__, PATHINFO_BASENAME));
//echo "</br>";


// Echo done
echo date('H:i:s') , " Proceso de escritura finalizado";
echo "</br>";
echo 'Tiempo tomado en la escritura del fichero ' , sprintf('%.4f',$callTime) , " seconds";
echo "</br>";
echo ' Uso de memoria: ' , (memory_get_usage(true) / 1024 / 1024) , " MB";
echo "</br>"; 
$ruta07="Analisis_Tecnicos/".$carpeta."/".$nomfichero2007;
//$url07=rawurlencode($ruta07);
//echo $ruta;
echo 'Ficheros guardados en <b><a href='.$ruta07.' >'.$ruta07.'</a></b>';
echo "</br>";
}

// Guardar excel en formato Excel 95
if ($excel95){

echo "</br>";
echo date('H:i:s') , " Escribe en formato Excel 95";
$callStartTime = microtime(true);
echo "</br>";

$nombrefichero95="Analisis_tecnico_".$nomens.".xls";//Nombre del documento
$carpeta95=$xml->assembly['defaultBankChannel'];
//echo __DIR__ ."/Analisis Tecnicos/".$carpeta;
if (!file_exists(__DIR__ ."/Analisis_Tecnicos/".$carpeta95)){
mkdir(__DIR__ ."/Analisis_Tecnicos/".$carpeta95,0700);}
$pathfichero95="/Analisis_Tecnicos/".$carpeta95."/".$nombrefichero95;//Ruta para guardar el documento

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save(str_replace('exportaexcell.php',$pathfichero95,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

//echo date('H:i:s') , " Fichero guardado " , str_replace('exportaexcell.php',$nombrefichero95, pathinfo(__FILE__, PATHINFO_BASENAME));
//echo "</br>";

// Echo done
echo date('H:i:s') , " Proceso de escritura finalizado";
echo "</br>";
echo 'Tiempo tomado en la escritura del fichero ' , sprintf('%.4f',$callTime) , " seconds";
echo "</br>";
echo ' Uso de memoria: ' , (memory_get_usage(true) / 1024 / 1024) , " MB";
echo "</br>";

$ruta95="Analisis_Tecnicos/".$carpeta95."/".$nombrefichero95;
//$url07=rawurlencode($ruta07);
//echo $ruta;
echo 'Ficheros guardados en <b><a href='.$ruta95.' >'.$ruta95.'</a></b>';
}

function pestrfc ($objPHPExcel) {

//Creamos la pestaña rfc ssmm
//echo date('H:i:s')," Escribiendo pestaa rfc ssmm";
//echo "</br>";
$pstrfcssmm  = $objPHPExcel->createSheet();
$pstrfcssmm ->setTitle('RFC SSMM');
//Seleccionamos como pestaña activa la ssmm
$objPHPExcel->setActiveSheetIndex(1);

//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('C00000');
$colrojo=array(
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),	
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C00000')
            ),
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 10,
			'name'  => 'Calibri'
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				)
		);
		


//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($colrojo);
//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
//Ponemos las etiquetas
$titec = utf8_encode('TECNOLOGÍA');
$tides = utf8_encode('DESCRIPCIÓN');
$objPHPExcel->setActiveSheetIndex(1)
            ->setCellValue('A1', $titec)
            ->setCellValue('B1', 'ENTREGA DE SOFTWARE')
			->setCellValue('C1', $tides)
            ->setCellValue('D1', 'ADJUNTOS');
		

//Creamos la pestaña host
//echo date('H:i:s') , " Escribiendo pestaña rfc host" , PHP_EOL;
//echo "</br>";
$pstrfchost = $objPHPExcel->createSheet();
$pstrfchost ->setTitle('RFC GIPIH');
//Seleccionamos como pestaña activa la de host
$objPHPExcel->setActiveSheetIndex(2);
//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('C00000');
//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($colrojo);
//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
//Ponemos las etiquetas
$titec = utf8_encode('TECNOLOGÍA');
$tides = utf8_encode('DESCRIPCIÓN');
$objPHPExcel->setActiveSheetIndex(2)
            ->setCellValue('A1', 'GIPIH')
            ->setCellValue('B1', 'RELATED DOCUMENTATION')
			->setCellValue('C1', 'DISHOST');

}

//Borra las tablas usadas y los archivos temporales
function deletetemp($conexion,$nombreFichero){

//Borrado de tablas
deltetbtmp("localizador",$conexion);
deltetbtmp("consultas",$conexion);
deltetbtmp("agrupacion",$conexion);
deltetbtmp("agrupacion_tmp",$conexion);
deltetbtmp("agrup_perm",$conexion);
deltetbtmp("publicacion",$conexion);
deltetbtmp("ttpp",$conexion);
deltetbtmp("ttpp_tmp",$conexion);

//Borrado de ficheros temporales
$ifile ="/isban/tmp/".$nombreFichero; // this is the actual path to the file you want to delete.
//echo $ifile;
unlink($_SERVER['DOCUMENT_ROOT'] .$ifile);
}

function deltetbtmp ($tabla,$conexion) {
	$consulta = "TRUNCATE TABLE ".$tabla;
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "$tabla";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "$tabla";
		print "existe cursor";
		print "$consulta";
	}
}

function numcomp($conexion,$xml,$producto,$publicacion){
$numwsexp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
$numwscon=count($xml->webServices->webServiceStates->webServiceState);//Numero de ws consumidos
$numtrxop=count($xml->communication->trxOp->trxOpStates->trxOpState);//Numero de transacciones partenon
$numsat = count($xml->communication->sat->satStates->satState);//Numero de sat
$numtrxalt=count($xml->communication->altair->altairStates->altairState);//Numero de transacciones altair

//echo "numwsexp:".$numwsexp;
//echo "numwscon:".$numwscon;
//echo "numtrxop=".$numtrxop;
//echo "numsat".$numsat;
//echo "numtrxalt".$numtrxalt;

$consulta = "INSERT INTO publicacion (IDPUB,PRODUCTO,WSEXP,WSCON,TTPP,TRXOP,SAT,ALTAIR,PERMISOS) VALUES ('$publicacion','$producto',$numwsexp,$numwscon,0,$numtrxop,$numsat,$numtrxalt,0)";
$cursor=mysqli_query($conexion,$consulta);
//print "$consulta";
//echo "<br/>";
if(mysqli_error($conexion)) {
	   print "La siguiente consulta no ha podido ser procesada:";
	   //echo "</br>";
	   echo "$consulta";
	   //echo "</br>";
	   }
}


function pintarNumComp($objPHPExcel,$consulta,$conexion,$publicacion){
$consulta = "SELECT * FROM publicacion WHERE IDPUB='$publicacion'";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
		//print "alta";
	}
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$numwsexp = $datos[2];
			$numwscon = $datos[3];
			$numttpp = $datos[4];
			$numtrxop = $datos[5];
			$numsat = $datos[6];
			$numtrxalt = $datos[7];
			$numperm = $datos[8];
			}}

	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('F10',$numwsexp)
            ->setCellValue('F11',$numwscon)
			->setCellValue('F12',$numttpp)
            ->setCellValue('F13',$numtrxop)
			->setCellValue('F14',$numsat)
            ->setCellValue('F15',$numtrxalt)
			->setCellValue('F16',$numperm);
			
			
}

function perLocalizadores($objPHPExcel,$coltitulo,$coltabla,$xml,$conexion,$doc,$nombreFichero,$publicacion){
	
	//Creamos la pestaña localizadores
//	echo date('H:i:s') , " Escribiendo pestaña localizadores";
//	echo "</br>";
	$pstperm = $objPHPExcel->createSheet();
	$pstperm ->setTitle('Permisos LOCs');

	//Seleccionamos como pestaña activa la de ws expuestos
	$objPHPExcel->setActiveSheetIndex(9);

	//Cambiamos el color de la pestaña
	$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

	//Formato Titulos
	$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($coltitulo);

	//Cambia el ancho de las columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);


	$nuWsExp=count($xml->channelAdapters->channelAdapter);//Numero de permisos loc
	//Ponemos las etiquetas
	$objPHPExcel->setActiveSheetIndex(9)
            ->setCellValue('A1', 'TABLA')
            ->setCellValue('B1', 'PERMISO')
			->setCellValue('C1', 'LOCALIZADOR');
	
	//Procesar localizadores
	$i=0;
	$nuPermLoc=count($xml->sqlComponents->sqlComponent);
	//echo $nuPermLoc;	
	if ($nuPermLoc>0){//Si tiene componentes sql
		//filas
		foreach($xml->sqlComponents->sqlComponent as $sqlComponent) {
		//Tabla de localizadores
		$localizadores=$sqlComponent->parameters->sqlComponentParams;
		$module=$localizadores->module;
		$component=$localizadores->component;
		$dataSourceAlias=$localizadores->dataSourceAlias;
				
		//Insertamos en la tabla localizador
		grabarLoc($conexion,$nombreFichero,$module,$component,$dataSourceAlias);
		
			
		
		//Insertamos en la tabla consultas
		foreach($sqlComponent->sqls->{'sql-sentence'} as $sentence) {
			$tablas=$sentence->sql;
			$tablas = strtoupper($tablas);
			$module=$sentence->module;
			$component=$sentence->component;
			//echo $tablas;
			//echo"<br/>";
			grabarComp ($conexion,$nombreFichero,$module,$component,$tablas,$i);
			$i++;
			}
		}
	}
	
	include("procesarPermisos.php");	

	//Pintar localizadores
	$consulta =  "SELECT tabla,permiso,localizador FROM agrup_perm WHERE nombreFichero='$nombreFichero' order by tabla";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
		if(!$cursor) {
		//print "alta";
		}
		else {
			$n=mysqli_num_rows($cursor);
			$consulta2 = "UPDATE publicacion SET PERMISOS=$n WHERE IDPUB='$publicacion'";
			$cursor2=mysqli_query($conexion,$consulta2);
			//print "$consulta2";

			if(mysqli_error($conexion)) {
			print "La siguiente consulta no ha podido ser procesada:";
			echo "</br>";
			echo "$consulta2";
			echo "</br>";}	
		
		if ($n>0){
		//filas
		$row = 2;	
		while($datos=mysqli_fetch_array($cursor)){
			$tabla=$datos[0];
			$permisos=$datos[1];
			$localizador=$datos[2];
			//echo "$tabla";
			//echo "$permisos";
			//echo "localizador";
			//Formato tabla
			$objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$row)->applyFromArray($coltabla);
			//Escribimos los valores
			$objPHPExcel->setActiveSheetIndex(9)->setCellValue('A'.$row,$tabla);
			$objPHPExcel->setActiveSheetIndex(9)->setCellValue('B'.$row,$permisos);
			$objPHPExcel->setActiveSheetIndex(9)->setCellValue('C'.$row,$localizador);
			$row=$row+1;
			}	
		
		
		}else{//Si no tiene
		$objPHPExcel->setActiveSheetIndex(9)->setCellValue('A2','NO TIENE');//No tiene	
		}
	}
		

}

function grabarComp($conexion,$nombreFichero,$moduloNegocio, $componente, $tablas,$i) {
	
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$tablas=trim($tablas);
	
	//$tablas = strtoupper($tablas);
    $tipoConsulta = substr($tablas, 0, 1);

	//Select
	if ($tipoConsulta=='S') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
		if ($posWhere=="") {
		$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	//Insert
	if ($tipoConsulta=='I') {
		$permisos="INSERT";
		$posFrom = strpos($tablas, "INTO");
		$posWhere = strpos($tablas, "(");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("INTO", "", $tablasBBDD);
	}
	//Update
	if ($tipoConsulta=='U') {
		$permisos="UPDATE";
		$posFrom = strpos($tablas, "UPDATE");
		$posWhere = strpos($tablas, "SET");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("UPDATE", "", $tablasBBDD);
	}
	//Delete
	if ($tipoConsulta=='D') {
		$permisos="DELETE";
		$posFrom = strpos($tablas, "FROM");
		//print "*A*$posFrom**";
		$posFrom=$posFrom+4;
		//print "*D*$posFrom**";
		$posWhere = strpos($tablas, "WHERE");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	
	//With temp
	if ($tipoConsulta=='W') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strpos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	//echo "$tablasBBDD";
		
	//Si hay un segundo from
	$tablasBBDD2 = substr($tablas, $posWhere,strlen($tablas));
	$posFrom = strpos($tablasBBDD2, "FROM");
	if ($posFrom!=""){
	$posFrom=$posFrom+4;
	$posWhere = strpos($tablasBBDD2, "WHERE");
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD2 = substr($tablasBBDD2, $posFrom, $posWhere);
	}
	//echo "$tablasBBDD2";
	
	if ($tablasBBDD2!=""){
		$tablasBBDD=$tablasBBDD.",".$tablasBBDD2;}
	//$posAs=strpos($tablasBBDD, " AS");
	//$tablasBBDD = substr($tablasBBDD,0,$posAs);
	//echo "$tablasBBDD";
	//echo "</br>";
	}
	
	
	$tablasBBDD = str_replace("$", "", $tablasBBDD);
	$tablasBBDD = str_replace("SCHEMA.", "", $tablasBBDD);
	$tablasBBDD = str_replace("'", "", $tablasBBDD);
	if (trim($tablasBBDD)=="") {
			$tablasBBDD = $tablas;
	}
	
	//echo $tablasBBDD;
	//echo "</br>";
	//print "$i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos'";
	
	$consulta = "INSERT INTO consultas (fila,nombreFichero,modulo_negocio, componente, tablas,permiso) 
	VALUES ($i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos')";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	//echo "<br/>";
	if(mysqli_error($conexion)) {
	   print "La siguiente consulta no ha podido ser procesada:";
	   echo "</br>";
	   echo "$tablasBBDD";
	   echo "</br>";
	}
}

//Tablas de parámetros
function tablasParametros($objPHPExcel,$coltitulo,$coltabla,$xml,$conexion,$nombreFichero,$publicacion){
		
	//Creamos la pestaña TTPP
	//echo date('H:i:s') , " Escribiendo pestaña ttpp" , PHP_EOL;
	//echo "</br>";
	$pstwsexp = $objPHPExcel->createSheet();
	$pstwsexp  ->setTitle('TTPP');

	//Seleccionamos como pestaña activa la de ws expuestos
	$objPHPExcel->setActiveSheetIndex(5);

	//Cambiamos el color de la pestaña 
	$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

	//Formato Titulos
	$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->applyFromArray($coltitulo);

	//Cambia el ancho de las columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);


	$nuWsExp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
	//Ponemos las etiquetas
	$tiverrel = utf8_encode('VERSIÓN / RELEASE');
	$objPHPExcel->setActiveSheetIndex(5)
            ->setCellValue('A1', 'TABLA')
            ->setCellValue('B1', $tiverrel)
			->setCellValue('C1', 'LOCALIZADOR');
	
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
		
	foreach($xml->global->treeTbls->treeTbl as $cacheadas) {
			$tabla=$cacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla cacheada:".$tabla;
			grabarTTPP($conexion,$nombreFichero,'ttpp_tmp',$tabla,'','');
			}
			
	//no cacheadas
	foreach($xml->global->mgrTables->mgrTable as $nocacheadas) {
			$tabla=$nocacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla no cacheada:".$tabla;
			grabarTTPP($conexion,$nombreFichero,'ttpp_tmp',$tabla,'','');
			
			}
	
	include("procesarTTPP.php");
	
	$consulta =  "SELECT TABLA,VERSION,LOCALIZADOR FROM ttpp WHERE nombreFichero='$nombreFichero' order by TABLA";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	
	if(!$cursor) {
		//print "alta";
	}
	else {
		$n=mysqli_num_rows($cursor);
		$consulta2 = "UPDATE publicacion SET TTPP=$n WHERE IDPUB='$publicacion'";
		$cursor2=mysqli_query($conexion,$consulta2);
		//print "$consulta";

		if(mysqli_error($conexion)) {
		print "La siguiente consulta no ha podido ser procesada:";
		echo "</br>";
		echo "$consulta2";
		echo "</br>";}	
		
		if ($n>0){
		//filas
		$row = 2;	
		while($datos=mysqli_fetch_array($cursor)){
			$tabla=$datos[0];
			$version=$datos[1];
			$localizador=$datos[2];
			//Formato tabla
			$objPHPExcel->getActiveSheet()->getStyle("A".$row.":C".$row)->applyFromArray($coltabla);
			//Escribimos los valores
			$objPHPExcel->setActiveSheetIndex(5)->setCellValue('A'.$row,$tabla);
			$objPHPExcel->setActiveSheetIndex(5)->setCellValue('B'.$row,$version);
			$objPHPExcel->setActiveSheetIndex(5)->setCellValue('C'.$row,$localizador);
			$row=$row+1;
			}	
		
		
		}else{//Si no tiene
		$objPHPExcel->setActiveSheetIndex(5)->setCellValue('A2','NO TIENE');//No tiene	
		}
	}
	
  

	
}

//Graba la ttpp
function grabarTTPP($conexion,$nombreFichero,$tdestino,$tabla,$version,$localizador) {
	//print "grabarTTPPIni";
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO $tdestino (TABLA,VERSION,LOCALIZADOR,nombreFichero) 
	VALUES ('$tabla', '$version', '$localizador','$nombreFichero')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	
	if(mysqli_error($conexion)) {
	  print "$consulta<br/>";
	  print "grabarTTPP-".$tdestino."-mysql_error<br/>";	
	}
}

	


//Graba la tabla inicial de localizadores
function grabarLoc($conexion,$nombreFichero,$moduloNegocio, $componente, $alias) {
	//print "Grabar";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO localizador (nombreFichero,modulo_negocio, componente, alias) 
	VALUES ('$nombreFichero','$moduloNegocio', '$componente', '$alias')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "grabarLoc-mysql_error";	
	}
}



//Datos Generales
function datosGenerales($objPHPExcel,$xml,$producto,$publicacion,$entrega,$rfc,$rijel,$usabilidad,$arq,$prop){
	
	//echo date('H:i:s') , " Obteniendo Datos Generales" , PHP_EOL;
	$callStartTime = microtime(true);
	//echo "</br>";
	
	//Obtenemos los datos
	$nomens=$xml->assembly['name'];//Nombre del ens
	$confens=$xml->assembly['defaultBankChannel'];//Canal configuración
	
	//Canal 
	foreach($xml->assembly->aebMultis->multisCategories as $multiscat){
	$gama=$multiscat->category[0];
	$idioma=$multiscat->category[1];
	$empresa=$multiscat->category[2];
	$canal=$multiscat->category[3];
	}
	
	//Comprobar auditoria
	$audit="No";
	foreach($xml->assembly->assemblyLogLevelsDefinitions->category as $category){
	$prioridad=$category->level;
	if ($prioridad='Audit'){
	$audit="Si";
	break;}
	}
	
	//Comprobar ws
	$ws="No";
	$nuWsExp=count($xml->channelAdapters->channelAdapter);
	$nuWsCon=count(count($xml->webServices->webServiceStates->webServiceState));
	if ($nuWsEx>0 or $nuWsCon>0){//No tiene
	$ws="Si";
	}
	
	//Comprobar comunicaciones
	$protocolo=$xml->communication->trxOp->trxOpProtocol;
	$modo=$xml->communication->trxOp->trxOpDefaultMode;
	$redg=$xml->communication->trxOp->trxOpRedGProtocol;
	$tcpalias=$xml->communication->trxOp->trxOpTCPAlias;
	$protsat=$xml->communication->sat->satProtocol;
	$aliassat=$xml->communication->sat->satAlias;
	$canallog=$xml->communication->sat->satLogicalChannels;
	$canalfis=$xml->communication->sat->satPyshicalChannels;
	
	// Cambiamos el nombre de la pestaña
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('General');
	//Mostrar la cuadrícula 
    $objPHPExcel->getActiveSheet()->setShowGridlines(false);
	//Cambiamos el color de la pestaña 
	$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('0070C0');
							
	//Columna azul con letras blancas
	$colazul=array(
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),	
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '0070C0')
            ),
			'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => 'FFFFFF'),
			'size'  => 10,
			'name'  => 'Calibri'
			)
		);
    
   
	
	//Bordes blancos
	$bordesblancos=array(
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),	
        );
    
	
	//Columna azul claro
	$azulclaro=array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				),
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'DDEBF7')
            ),
			'font'  => array(
			'size'  => 10,
			'name'  => 'Calibri'
			)
        );
    
	
	$coltitulo=array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				),
			'borders' => array(
				'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => 'FFFFFF'),
				),
			),
            'font'  => array(
			'bold'  => true,
			'size'  => 10,
			'name'  => 'Calibri'
			)
        );
		
	$objPHPExcel->getActiveSheet()->getRowDimension('1:32')->setRowHeight(-2);
	$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-2);
	
	//Datos de entrega
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B2', 'DATOS ENTREGA')
            ->setCellValue('B3', 'Nombre SGS:')
			->setCellValue('C3', $producto)
            ->setCellValue('B4', 'Ensamblado:')
			->setCellValue('C4', $nomens)
            ->setCellValue('B5', 'Zip:')
			->setCellValue('C5', $entrega)
			->setCellValue('B6', 'Baseline:')
			->setCellValue('C6', $publicacion)
			->setCellValue('B7', 'RFC:')
			->setCellValue('C7', $rfc);
			
	//Titulos
	$objPHPExcel->getActiveSheet()->getStyle('B3:B7')->applyFromArray($colazul);
	//Valores
	$objPHPExcel->getActiveSheet()->getStyle('C3:C7')->applyFromArray($azulclaro);
		
	//Cambia el ancho de las columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
	

	//Titulos
	//Hace colspan de dos celdas
	$objPHPExcel->getActiveSheet()->mergeCells('B2:C2');
	$objPHPExcel->getActiveSheet()->mergeCells('B9:C9');
	$objPHPExcel->getActiveSheet()->mergeCells('B16:C16');
	$objPHPExcel->getActiveSheet()->mergeCells('B22:C22');
	$objPHPExcel->getActiveSheet()->mergeCells('E2:F2');
	$objPHPExcel->getActiveSheet()->mergeCells('E9:F9');
	$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($coltitulo);
	$objPHPExcel->getActiveSheet()->getStyle('B9')->applyFromArray($coltitulo);
	$objPHPExcel->getActiveSheet()->getStyle('B16')->applyFromArray($coltitulo);
	$objPHPExcel->getActiveSheet()->getStyle('B22')->applyFromArray($coltitulo);
	$objPHPExcel->getActiveSheet()->getStyle('E2')->applyFromArray($coltitulo);
	$objPHPExcel->getActiveSheet()->getStyle('E9')->applyFromArray($coltitulo);
	
    //Datos Generales
	$ticonf = utf8_encode('Configuración:');
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B9', 'DATOS GENERALES')
            ->setCellValue('B10', 'Auditoria:')
			->setCellValue('C10', $audit)
            ->setCellValue('B11', $ticonf)
			->setCellValue('C11', $confens)
            ->setCellValue('B12', 'Gama Perfil:')
			->setCellValue('C12', $gama)
			->setCellValue('B13', 'Idioma:')
			->setCellValue('C13', $idioma)
			->setCellValue('B14', 'Empresa:')
			->setCellValue('C14', $empresa)
			->setCellValue('B14', 'Canal Marco:')
			->setCellValue('C14', $canal)
			->setCellValue('B14', 'Web Services:')
			->setCellValue('C14', $ws);
			
	//Etiquetas
	$objPHPExcel->getActiveSheet()->getStyle('B10:B14')->applyFromArray($colazul);
	//Valores
	$objPHPExcel->getActiveSheet()->getStyle('C9:C14')->applyFromArray($azulclaro);
        
	
	//Datos Instalación
	$tiins = utf8_encode('DATOS INSTALACIÓN');
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B16', $tiins)
            ->setCellValue('B17', 'Rigel:')
			->setCellValue('C17', $rijel)
            ->setCellValue('B18', 'Globales Usabilidad:')
			->setCellValue('C18', $usabilidad)
            ->setCellValue('B19', 'Globales Aquitectura')
			->setCellValue('C19', $arq)
			->setCellValue('B20', 'Properties')
			->setCellValue('C20', $prop);
			
	//Etiquetas
	$objPHPExcel->getActiveSheet()->getStyle('B17:B20')->applyFromArray($colazul);
    //Valores
	$objPHPExcel->getActiveSheet()->getStyle('C17:C20')->applyFromArray($azulclaro);    
	
	
	
	//Comunicaciones
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B22', 'COMUNICACIONES')
            ->setCellValue('B23', 'Protocolo TRXOP:')
			->setCellValue('C23',$protocolo)
            ->setCellValue('B24', 'Modo TRXOP:')
			->setCellValue('C24',$modo)
            ->setCellValue('B25', 'Protocolo RedG:')
			->setCellValue('C25',$redg)
			->setCellValue('B26', 'TCP Alias:')
			->setCellValue('C26',$tcpalias)
			->setCellValue('B27', 'Protocolo SAT:')
			->setCellValue('C27',$protsat)
			->setCellValue('B28', 'Alias SAT:')
			->setCellValue('C28',$aliassat)
			->setCellValue('B29', 'Canal Logico:')
			->setCellValue('C29',$canallog)
			->setCellValue('B30', 'Canal Fisico:')
			->setCellValue('C30',$canalfis);
			
	//Etiquetas
	$objPHPExcel->getActiveSheet()->getStyle('B23:B30')->applyFromArray($colazul);
    //Valores
	$objPHPExcel->getActiveSheet()->getStyle('C23:C30')->applyFromArray($azulclaro);   
        
	//Tecnologias asociadas
	$titec = utf8_encode('TECNOLOGÍAS ASOCIADAS');
	$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('E2', $titec)
            ->setCellValue('E3', 'Content Manager:')
			->setCellValue('F3','NO')
            ->setCellValue('E4', 'Content Catalog:')
			->setCellValue('F4','NO')
            ->setCellValue('E5', 'PI:')
			->setCellValue('F5','NO')
			->setCellValue('E6', 'Stafware:')
			->setCellValue('F6','NO')
			->setCellValue('E7', 'iLog:')
			->setCellValue('F7','NO');
			
	//Etiquetas
	$objPHPExcel->getActiveSheet()->getStyle('E3:E7')->applyFromArray($colazul);
    //Valores
	$objPHPExcel->getActiveSheet()->getStyle('F3:F7')->applyFromArray($azulclaro);  

	//Inventario de componentes
	$tiinv = utf8_encode('INVENTARIO DE COMPONENTES');
	$tittpp = utf8_encode('Tablas de parámetros:');
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E9', $tiinv)
            ->setCellValue('E10','Web Services Expuestos:')
            ->setCellValue('E11','Web Services Consumidos:')
			->setCellValue('E12',$tittpp)
            ->setCellValue('E13','Trx-Op Partenon:')
			->setCellValue('E14','Sat:')
            ->setCellValue('E15','Trx Altair:')
			->setCellValue('E16','Tablas en comp. sql:');

			
	//Etiquetas
	$objPHPExcel->getActiveSheet()->getStyle('E10:E16')->applyFromArray($colazul);
    //Valores
	$objPHPExcel->getActiveSheet()->getStyle('F10:F16')->applyFromArray($azulclaro);  	
        
 }

//TrxOP Partenon
function trxOP($objPHPExcel,$coltitulo,$coltabla,$xml,$publicacion){

//Creamos la pestaña trxop partenon
//echo date('H:i:s') , " Escribiendo pestaña TrxOP Partenon";
//echo "</br>";
$pstwsexp = $objPHPExcel->createSheet();
$pstwsexp  ->setTitle('TrxOP Partenon');

//Seleccionamos como pestaña activa la de ws expuestos
$objPHPExcel->setActiveSheetIndex(6);

//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($coltitulo);

//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

//Ponemos las etiquetas
$tiapl = utf8_encode('APLICACIÓN');
$tioperi = utf8_encode('OPERACIÓN INTERNA');
$titrx = utf8_encode('TRANSACCIÓN');
$tioper= utf8_encode('OPERACIÓN');
$tiver= utf8_encode('VERSIÓN');
$objPHPExcel->setActiveSheetIndex(6)
            ->setCellValue('A1', $tiapl)
            ->setCellValue('B1', $tioperi)
			->setCellValue('C1', $titrx)
            ->setCellValue('D1', $tioper)	
			->setCellValue('E1', $tiver);	

	//Partenon
	$numTrxOp=count($xml->communication->trxOp->trxOpStates->trxOpState);
	
	if ($numTrxOp>0){//Si tiene trx
		//filas
		$row = 2;
		
		foreach($xml->communication->trxOp->trxOpStates->trxOpState as $trxOpState) {
		$aplicacion=$trxOpState->application;
		$oi=$trxOpState->internalOp;
		$trx=$trxOpState->transaction;
		$operacion=$trxOpState->operation;
		$version=$trxOpState->version;
		//Formato tabla
		$objPHPExcel->getActiveSheet()->getStyle("A".$row.":E".$row)->applyFromArray($coltabla);
		//Escribimos los valores
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('A'.$row,$aplicacion);
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('B'.$row,$oi);
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('C'.$row,$trx);
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('D'.$row,$operacion);
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('E'.$row,$version);
		$row=$row+1;}
				
	}else{//Si no tiene
		$objPHPExcel->setActiveSheetIndex(6)->setCellValue('A2','NO TIENE');//No tiene	
	}

//Sat
//Creamos la pestaña sat
//echo date('H:i:s') , " Escribiendo pestaña SAT";
//echo "</br>";
$pstwsexp = $objPHPExcel->createSheet();
$pstwsexp  ->setTitle('SAT');

//Seleccionamos como pestaña activa la de ws expuestos
$objPHPExcel->setActiveSheetIndex(7);

//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($coltitulo);

//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

//Ponemos las etiquetas
$tiapl = utf8_encode('APLICACIÓN');
$tioperi = utf8_encode('OPERACIÓN INTERNA');
$tiver= utf8_encode('VERSIÓN');
$objPHPExcel->setActiveSheetIndex(7)
            ->setCellValue('A1', $tiapl)
            ->setCellValue('B1', $tioperi)
			->setCellValue('C1', 'NOMBRE')
			->setCellValue('D1', $tiver);
	
	$kids = $xml->communication->sat->satStates->children();
	$numSat=count($kids);
	//echo $numSat;
	
	if ($numSat>0){//Si tiene trx
	
		//filas
		$row = 2;
		
		//filas
		foreach($xml->communication->sat->satStates->satState as $satState) {
		$aplicacion=$satState->application;
		$oi=$satState->internalOp;
		$trx=$satState->satName;
		$version=$satState->version;
		
		if ($trx!=""){
		//Formato tabla
		$objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($coltabla);
		//Escribimos los valores
		$objPHPExcel->setActiveSheetIndex(7)->setCellValue('A'.$row,$aplicacion);
		$objPHPExcel->setActiveSheetIndex(7)->setCellValue('B'.$row,$oi);
		$objPHPExcel->setActiveSheetIndex(7)->setCellValue('C'.$row,$trx);
		$objPHPExcel->setActiveSheetIndex(7)->setCellValue('D'.$row,$version);
		$row=$row+1;
		}
		
		}
		
		
	}else{//Si no tiene
	$objPHPExcel->setActiveSheetIndex(7)->setCellValue('A2','NO TIENE');//No tiene	
	}
	
	//Altair
//Creamos la pestaña altair
//echo date('H:i:s') , " Escribiendo pestaña trxOP Altair";
//echo "</br>";
$pstwsexp = $objPHPExcel->createSheet();
$pstwsexp  ->setTitle('TrxOP Altair');

//Seleccionamos como pestaña activa la de ws expuestos
$objPHPExcel->setActiveSheetIndex(8);

//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($coltitulo);

//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

//Ponemos las etiquetas
$tiapl = utf8_encode('APLICACIÓN');
$tioperi = utf8_encode('OPERACIÓN INTERNA');
$titrx = utf8_encode('TRANSACCIÓN');
$tiver= utf8_encode('VERSIÓN');
$objPHPExcel->setActiveSheetIndex(8)
            ->setCellValue('A1', $tiapl)
            ->setCellValue('B1', $tioperi)
			->setCellValue('C1', $titrx)
			->setCellValue('D1', $tiver);
	
	
	$kids = $xml->communication->altair->altairStates->children();
	$numTrxAltair=count($kids);
	
	if ($numTrxAltair>0){//Si tiene trx
	
		$protaltair=$xml->communication->altair->altairProtocol;
		$altairalias=$xml->communication->altair->altairAlias;
		//filas
		$row = 2;
		//filas
		foreach($xml->communication->altair->altairStates->altairState as $altairState) {
		$aplicacion=$altairState->application;
		$oi=$altairState->internalOp;
		$trx=$altairState->transaction;
		$version=$altairState->version;
		
		//Formato tabla
		$objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($coltabla);
		//Escribimos los valores
		$objPHPExcel->setActiveSheetIndex(8)->setCellValue('A'.$row,$aplicacion);
		$objPHPExcel->setActiveSheetIndex(8)->setCellValue('B'.$row,$oi);
		$objPHPExcel->setActiveSheetIndex(8)->setCellValue('C'.$row,$trx);
		$objPHPExcel->setActiveSheetIndex(8)->setCellValue('D'.$row,$version);
		$row=$row+1;}
		
	}else{//Si no tiene
	$objPHPExcel->setActiveSheetIndex(8)->setCellValue('A2','NO TIENE');//No tiene	
	}
}



//WS Expuestos
function wsExpuestos($objPHPExcel,$coltitulo,$coltabla,$xml){

//Creamos la pestaña ws expuestos
//echo date('H:i:s') , " Escribiendo pestaña ws expuestos";
//echo "</br>";
$pstwsexp = $objPHPExcel->createSheet();
$pstwsexp  ->setTitle('WS Expuestos');

//Seleccionamos como pestaña activa la de ws expuestos
$objPHPExcel->setActiveSheetIndex(3);

//Cambiamos el color de la pestaña
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($coltitulo);

//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);

$nuWsExp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
//Ponemos las etiquetas
$objPHPExcel->setActiveSheetIndex(3)
            ->setCellValue('A1', 'ADAPTADOR')
            ->setCellValue('B1', 'TIPO')
			->setCellValue('C1', 'FACHADA')
            ->setCellValue('D1', 'ALIAS');	
	
if ($nuWsExp>0){//Si tiene ws
			
		//filas
		$row = 2;
		foreach($xml->channelAdapters->channelAdapter as $channelAdapter) {
		$adaptador=$channelAdapter->adapterName;
		$tipo=$channelAdapter->type;
		$fachada=$channelAdapter->facadeName;
		$alias=$channelAdapter->alias;
		
		//Formato tabla
		$objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($coltabla);
		//Escribimos los valores
		$objPHPExcel->setActiveSheetIndex(3)->setCellValue('A'.$row,$adaptador);
		$objPHPExcel->setActiveSheetIndex(3)->setCellValue('B'.$row,$tipo);
		$objPHPExcel->setActiveSheetIndex(3)->setCellValue('C'.$row,$fachada);
		$objPHPExcel->setActiveSheetIndex(3)->setCellValue('D'.$row,$alias);
		$row=$row+1;
		}
	}else{//Si no tiene
	//Ponemos las etiquetas
	$objPHPExcel->setActiveSheetIndex(3)->setCellValue('A2','NO TIENE');//No tiene
	}
	
}


function wsConsumidos($objPHPExcel,$coltitulo,$coltabla,$xml){

//Creamos la pestaña ws consumidos
//echo date('H:i:s') , " Escribiendo pestaña ws consumidos";
//echo "</br>";
$pstwscon = $objPHPExcel->createSheet();
$pstwscon ->setTitle('WS Consumidos');

//Seleccionamos como pestaña activa la de ws consumidos
$objPHPExcel->setActiveSheetIndex(4);

//Cambiamos el color de la pestaña 
$objPHPExcel->getActiveSheet()->getTabColor()->setRGB('305496');

//Formato Titulos
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($coltitulo);
//Cambia el ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$tiapl = utf8_encode('APLICACIÓN');
$tioper = utf8_encode('OPERACIÓN');
$objPHPExcel->setActiveSheetIndex(4)
            ->setCellValue('A1', $tiapl)
            ->setCellValue('B1', $tioper)
			->setCellValue('C1', 'ALIAS')
            ->setCellValue('D1', 'TIPO');
		
	$nuWsCon=count($xml->webServices->webServiceStates->webServiceState);
		
	if ($nuWsCon>0){//Si tiene ws
	//Ponemos las etiquetas
		
			//filas
			$row = 2;
			foreach($xml->webServices->webServiceStates->webServiceState as $wsstates) {
			$aplicacion=$wsstates->application;
			$operacion=$wsstates->internalOP;
			$alias=$wsstates->alias;
			$tipo=$wsstates->transport;
			
			//Formato tabla
			$objPHPExcel->getActiveSheet()->getStyle("A".$row.":D".$row)->applyFromArray($coltabla);
			//Escribimos los valores
			$objPHPExcel->setActiveSheetIndex(4)->setCellValue('A'.$row,$aplicacion);
			$objPHPExcel->setActiveSheetIndex(4)->setCellValue('B'.$row,$operacion);
			$objPHPExcel->setActiveSheetIndex(4)->setCellValue('C'.$row,$alias);
			$objPHPExcel->setActiveSheetIndex(4)->setCellValue('D'.$row,$tipo);
			$row=$row+1;
			
			}
		
			
	}else{//Si no tiene
		$objPHPExcel->setActiveSheetIndex(4)->setCellValue('A2','NO TIENE');//No tiene
	}
	
}


function catalogoContenidos($objPHPExcel,$xml){
	
	$catcon=$xml->other->catCon;
	if ($catcon=="no"){
		$objPHPExcel->setActiveSheetIndex(1)->setCellValue('F4','NO');//No tiene
	}else{
		$objPHPExcel->setActiveSheetIndex(1)->setCellValue('F4','SI');//Si tiene
}



}

?>
