﻿<?php
//2016 - Manuel Llamas Gómez - Pool de implantación
$nomFichpro=obtnomfich($conexion);
insAgrupacionIni($conexion,$nomFichpro);
separaTablas($conexion,$nomFichpro);
agrupPermisos($conexion,$nomFichpro);
//pintarDatos($conexion,$nomFichpro);

function obtnomfich($conexion) {
$consulta="SELECT distinct nombreFichero FROM consultas";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor obtnomfich"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
		return $datos[0];
		//echo "Nombre del fichero: $nomFichpro";
		}}
}


//Separamos las tablas en las celdas
function agrupPermisos($conexion,$nomFichpro)
{
//print "agrupPermisos";
$consulta="SELECT distinct tabla FROM agrupacion_tmp";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor agrupPermisos"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = $datos[0];
			//print "tabla=$tabla";
			$localizador=obtLocTabla($conexion,$tabla);
			$permiso=obtPermTabla($conexion,$tabla);
			insAgrupacion ("agrup_perm",$conexion,$nomFichpro,$tabla,$permiso,$localizador);//Inserta el registro
			}
			
		}	
}

//Inicializa la tabla de agrupaciones
function insAgrupacionIni($conexion,$nomFichpro) {
//print "insAgrupacionIni";
$consulta =  "SELECT fila,tablas,permiso,alias,b.nombreFichero FROM consultas a,localizador b where a.modulo_negocio =b.modulo_negocio and a.componente = b.componente and a.nombreFichero=b.nombreFichero 
and b.nombreFichero='$nomFichpro' order by tablas";
//print $consulta;	
$cursor=mysqli_query($conexion,$consulta);
if(!$cursor) {
	print "alta";
}
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$numero = $datos[0];
			$tablas=trim($datos[1]);
			$permiso=trim($datos[2]);
			$localizador=trim($datos[3]);
			$nomFichpro=trim($datos[4]);
			//print "tablas:$tablas**";
			//echo "</br>";
			if (strpos($tablas,'UNION SELECT') !== false) { //Tratamos los UNION SELECT
				$tablas=tratarJoins ('UNION SELECT',$tablas);
			}
			if (strpos($tablas,'SELECT') !== false) { //Tratamos los SUB-SELECT
				$tablas=trataSubSelect($tablas);
			}
			if (strpos($tablas,'INNER JOIN') !== false) { //Tratamos los INNER JOIN
				$tablas=tratarJoins ('INNER JOIN',$tablas);
			}
			if (strpos($tablas,'LEFT JOIN') !== false) {//Tratamos los LEFT JOIN
				$tablas=tratarJoins ('LEFT JOIN',$tablas);
			}
			if (strpos($tablas,'RIGHT JOIN') !== false) {//Tratamos los RIGHT JOIN
				$tablas=tratarJoins ('RIGHT JOIN',$tablas);
			}
			
			$tablas=trim($tablas);
			//Print "tabla=$tablas**";
			//echo "</br>";
			insAgrupacion ("agrupacion",$conexion,$nomFichpro,$tablas, $permiso, $localizador); 
		}	
	}
	

}

//Separamos las tablas en las celdas
function separaTablas($conexion,$nomFichpro)
{
//print "separaTablas";
$consulta="SELECT tabla,permiso,nombreFichero,localizador FROM agrupacion group by tabla,permiso";
$cursor=mysqli_query($conexion,$consulta);;
	if(!$cursor) {
	print "Error cursor vseparaTablas"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = trim($datos[0]);
			$permiso=$datos[1];
			$nomFichpro=$datos[2];
			$localizador=$datos[3];
			$elements = explode(",", $tabla);
				for ($ii = 0; $ii < count($elements); $ii++) {
				$nquotes = trim($elements[$ii]);//Elimina espacios al comienzo y al final
				if (strpos($nquotes," AS ") !== false) {//Si tiene clausula as
				$nquotes=strstr($nquotes ," AS ",true);}//Eliminamos la clausula as
				if (strpos($nquotes," ")!== false) {//Si tiene seudonimo
				$nquotes=strstr($nquotes," ",true);}//Eliminamos seudonimo
				$nquotes=trim($nquotes);
				if ($nquotes!=""){
				insAgrupacion ("agrupacion_tmp",$conexion,$nomFichpro,$nquotes,$permiso,$localizador);}
				}
			
		}	
	}
	
}

//Pinta los datos de permisos de localizadores
function pintarDatos($conexion,$nomFichpro) {
//print "pintar";
$consulta =  "SELECT tabla,permiso,localizador FROM agrup_perm WHERE nombreFichero='$nomFichpro' order by tabla";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);;
	if(!$cursor) {
		//print "alta";
	}
	else {
	    
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla=$datos[0];
			$permiso=$datos[1];
			$localizador=$datos[2];
			echo "<tr>";
			echo "<td>$tabla</td>";
			echo "<td>$permiso</td>";
			echo "<td>$localizador</td>";
			echo "</tr>";
		}	
		echo "</tbody></table></tr></td></table></br>";
	}
	
	echo "</br>";
	echo"<table><tr><td></td></tr></table></br>";	
	
}

function tratarJoins ($tipo,$tablas){

	//Extraemos tabla 1
	$tbl1=strstr($tablas ,"$tipo",true);
	if (strpos($tbl1,' ON ') !== false){//Comprobamos si tiene clausula ON
			$tbl1=strstr($tbl1," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl1,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tbl1=strstr($tbl1,"WHERE",true);//Extraemos solo la tabla
	}
	$tbl1=str_replace("$tipo","",$tbl1);//Eliminamos clausula
	//print "tabla 1-$tipo:$tbl1";
	
	
	//Extraemos tabla 2			
	$tbl2=strstr($tablas ,"$tipo",false);
	if (strpos($tbl2,' ON ') !== false){//Comprobamos si tiene clausula ON
		$tbl2=strstr($tbl2," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl2,'FROM') !== false){//Comprobamos si tiene clausula FROM (UNION)
			$tbl2=strstr($tbl2,"FROM",false);//Extraemos solo la tabla
			$tbl2=str_replace("FROM","",$tbl2);//Eliminamos clausula
	}
	$tbl2=str_replace("$tipo","",$tbl2);//Eliminamos clausula

	//print "tabla 2-$tipo:$tbl2";
	//echo "<br/>";
				
	$tablas=trim($tbl1).",".trim($tbl2);//Quitamos espacios y unimos por coma
	//echo "$tablas";
	return $tablas;
}

function trataSubSelect ($tablas){

	if (strpos($tablas,'FROM') !== false){//Comprobamos si tiene clausula FROM 
			$tablas=strstr($tablas,"FROM",false);//Extraemos solo la tabla
			$tablas=str_replace("FROM","",$tablas);//Eliminamos clausula
	}
	if (strpos($tablas,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tablas=strstr($tablas,"WHERE",true);//Extraemos solo la tabla
	}
	//print "trataSubSelect: $tablas";
	return $tablas;
}


//Inserta registros en las tablas de agrupaciones
function insAgrupacion ($tdestino,$conexion,$nomFichpro,$tabla, $permiso, $localizador) {
	//print "insAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "INSERT INTO $tdestino (tabla,nombreFichero, permiso, localizador) 
	VALUES ('$tabla', '$nomFichpro','$permiso', '$localizador')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysqli_error" ;
	}
}


//Borra registros en la tabla agrupación
function delAgrupacion ($conexion,$nomFichpro,$tabla,$permiso) {
	//print "delAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "DELETE FROM agrupacion where tabla='$tabla' and permiso='$permiso'";
	$cursor=mysqli_query($conexion,$consulta);;
	if(mysqli_error()) {
	   print "1 if Grabar";	
	   print "mysqli_error" ;
	}
}

//Obtiene los localizadores de cada tabla
function obtLocTabla($conexion,$tabla){
//print "obtLocTabla";
$consulta="SELECT DISTINCT localizador FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);;
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtLocTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$localizador=$datos[0];
		}
	}
	return $localizador;
}
	
//Obtiene los permisos de cada tabla
function obtPermTabla($conexion,$tabla)
{
//print "obtPermTabla";
$consulta="SELECT DISTINCT permiso FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);;
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtPermTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			if ($permiso!=""){
			$permiso=$permiso.",".$datos[0];}
			else{
			$permiso=$datos[0];}
		}
	}
	return $permiso;
}



?>