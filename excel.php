<?php
//Manuel Llamas G�mez - 2019-DevSecOps Platform - SGT IT Delivery
session_start();
include("BaseDatos.php");
error_reporting(E_ALL ^ E_NOTICE);
require_once 'Classes/PHPExcel.php';
$nombreFichero = $_SESSION['fichero']; //Nombre del fichero

$inputFileType = PHPExcel_IOFactory::identify($nombreFichero);


$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($nombreFichero);
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
$total=$highestRow;

//echo 'Nombre del fichero:'.$nombreFichero;
//echo 'tipo de archivo'.$inputFileType;
//echo 'Numero de filas'.$highestRow;
//echo 'Numero de columnas'.$highestColumn;

//Pesta�a Localizadores
for ($row = 2; $row <= $highestRow; $row++){ 
		$moduloNegocio= "".$sheet->getCell("A".$row)->getValue()."";
		$componente= "".$sheet->getCell("B".$row)->getValue()."";
		$alias= "".$sheet->getCell("E".$row)->getValue()."";
		grabarBD ($conexion,$nombreFichero,$moduloNegocio, $componente, $alias,$row);
		echo "\n";
}

//Pesta�a sql
$sheetsql = $objPHPExcel->getSheet(1);	
$highestRowsql = $sheetsql->getHighestRow(); 
$highestColumnsql = $sheetsql->getHighestColumn();
$totalsql=$highestRowsql;	
//echo 'Numero de filas: '.$totalsql;
//echo '</br>';

for ($row = 2; $row <= $highestRowsql; $row++){ 

		$moduloNegocio= "".$sheetsql->getCell("A".$row)->getValue()."";
		$componente= "".$sheetsql->getCell("B".$row)->getValue()."";
		$tablas= "".$sheetsql->getCell("E".$row)->getValue()."";
		grabarBD2 ($conexion,$nombreFichero,$moduloNegocio, $componente, $tablas,$row);
		echo "\n";

}

insAgrupacionIni ($conexion,$nombreFichero);
separaTablas($conexion);
agrupPermisos($conexion,$nombreFichero);
pintarDatos ($conexion,$nombreFichero);
delete ($conexion,$nombreFichero );
unlink($nombreFichero);


function delete ($conexion,$nombreFichero ) {
	//print "delete";
	$consulta = "TRUNCATE TABLE localizador";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE consultas";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	$consulta = "TRUNCATE TABLE agrupacion";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE agrupacion_tmp";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE agrup_perm";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
}
function procesarSelect($tablas) {
	
	return $tablasBBDDAux;
}

function grabarBD2 ($conexion,$nombreFichero,$moduloNegocio, $componente, $tablas,$row) {
	
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$tablas=trim($tablas);
	$tablas=strtoupper($tablas);
	
	$tipoConsulta = substr($tablas, 0, 1);
	//echo 'tablas:'.$tablas;
	//echo '</br>';
	//echo 'Tipo de consulta: '.$tipoConsulta;
	//echo '<br/>';

	//Select
	if ($tipoConsulta=='S') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
		if ($posWhere=="") {
		$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	
	//with temps
	else if ($tipoConsulta=='W') {
	$permisos="SELECT";
	$lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	//echo "with temp---$tablasBBDD";
	//echo "</br>";
	}
	
	//Insert
	else if ($tipoConsulta=='I') {
		$permisos="INSERT";
		$posFrom = strpos($tablas, "INTO");
		$posWhere = strpos($tablas, "(");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("INTO","",$tablasBBDD);

	}
	//Update
	else if ($tipoConsulta=='U') {
		$permisos="UPDATE";
		$posFrom = strpos($tablas, "UPDATE");
		$posWhere = strpos($tablas, "SET");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("UPDATE", "", $tablasBBDD);
	}
	//Delete
	else if ($tipoConsulta=='D') {
		$permisos="DELETE";
		$posFrom = strpos($tablas, "FROM");
		//print "*A*$posFrom**";
		$posFrom=$posFrom+4;
		//print "*D*$posFrom**";
		$posWhere = strpos($tablas, "WHERE");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	
	else if ($tipoConsulta=='W') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
	//$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	//echo "with temp---$tablasBBDD";
	//echo "</br>";
	}	
	
	$tablasBBDD = str_replace("$", "", $tablasBBDD);
	$tablasBBDD = str_replace("SCHEMA.", "", $tablasBBDD);
	$tablasBBDD = str_replace("'", "", $tablasBBDD);
	if (trim($tablasBBDD)=="") {
			$tablasBBDD = $tablas;
	}
	
	//echo $tablasBBDD;
	//echo "</br>";
	//print "$i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos'";
	
	$consulta = "INSERT INTO consultas (fila,nombreFichero,modulo_negocio, componente, tablas,permiso) 
	VALUES ($row,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos')";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	//echo "<br/>";
	if(mysqli_error($conexion)) {
	   print "La siguiente consulta no ha podido ser procesada:";
	   echo "</br>";
	   echo "$consulta";
	   echo "</br>";
	}
}

//Inicializa la tabla de agrupaciones
function insAgrupacionIni ($conexion,$nombreFichero) {
//print "insAgrupacionIni";
$consulta =  "SELECT fila,tablas,permiso,alias FROM consultas a,localizador b where a.modulo_negocio =b.modulo_negocio and a.componente = b.componente and a.nombreFichero=b.nombreFichero 
and b.nombreFichero='$nombreFichero' Order by tablas";
	//echo $consulta;
	$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
		//print "alta";
	}
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$numero = $datos[0];
			$tablas=trim($datos[1]);
			$permiso=trim($datos[2]);
			$localizador=trim($datos[3]);
			
			/*echo 'Numero:'.$numero;
			echo '</br>';
			echo 'Tablas:'.$tablas;
			echo '</br>';
			echo 'Permiso:'.$permiso;
			echo '</br>';
			echo 'Localizador:'.$localizador;
			echo '</br>';*/
								
			$tipJoin=consJoins($tablas);//Comprobamos clausulas
			//echo "Segundo ciclo:".$tablas;
			if($tipJoin!=null){//Mientras queden clausulas
				//echo "Segundo ciclo:$tipJoin";
				//echo $tablas;
				if ($tipJoin='SELECT'){//Si tenemos subselect
				$tablas=trataSubSelect($tablas);
				}else{//Si tenemos join
				$tablas=tratarJoins($tipJoin,$tablas);
				}
			}
			//Print "tabla=$tablas**";
			insAgrupacion ("agrupacion",$conexion,$nombreFichero,$tablas, $permiso, $localizador); 
		}	
	}
	

}

function consJoins ($tablas){

if (strpos($tablas,'UNION SELECT') !== false) { //Tratamos los UNION SELECT
return 'UNION SELECT';
}
if (strpos($tablas,'SELECT') !== false) { //Tratamos los SUB-SELECT
return 'SELECT';
}
if (strpos($tablas,'UNION SELECT') !== false) { //Tratamos los UNION SELECT
return 'UNION SELECT';
}
if (strpos($tablas,'INNER JOIN') !== false) { //Tratamos los INNER JOIN
return 'INNER JOIN';
}
if (strpos($tablas,'LEFT JOIN') !== false) {//Tratamos los LEFT JOIN
return 'LEFT JOIN';
}
if (strpos($tablas,'RIGHT JOIN') !== false) {//Tratamos los RIGHT JOIN
return 'RIGHT JOIN';
}
return null;
}

function tratarJoins ($tipo,$tablas){
	echo "tratarJoins";
	//Extraemos tabla 1
	$tbl1=strstr($tablas ,"$tipo",true);
	if (strpos($tbl1,' ON ') !== false){//Comprobamos si tiene clausula ON
			$tbl1=strstr($tbl1," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl1,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tbl1=strstr($tbl1,"WHERE",true);//Extraemos solo la tabla
	}
	$tbl1=str_replace("$tipo","",$tbl1);//Eliminamos clausula
	//print "tabla 1-$tipo:$tbl1";
	
	
	//Extraemos tabla 2			
	$tbl2=strstr($tablas ,"$tipo",false);
	if (strpos($tbl2,' ON ') !== false){//Comprobamos si tiene clausula ON
		$tbl2=strstr($tbl2," ON ",true);//Extraemos solo la tabla
	}
	if (strpos($tbl2,'FROM') !== false){//Comprobamos si tiene clausula FROM (UNION)
			$tbl2=strstr($tbl2,"FROM",false);//Extraemos solo la tabla
			$tbl2=str_replace("FROM","",$tbl2);//Eliminamos clausula
	}
	$tbl2=str_replace("$tipo","",$tbl2);//Eliminamos clausula

	//print "tabla 2-$tipo:$tbl2";
	//echo "<br/>";
				
	$tablas=trim($tbl1).",".trim($tbl2);//Quitamos espacios y unimos por coma
	//echo "$tablas";
	return $tablas;
}

function trataSubSelect ($tablas){

	if (strpos($tablas,'FROM') !== false){//Comprobamos si tiene clausula FROM 
			$tablas=strstr($tablas,"FROM",false);//Extraemos solo la tabla
			$tablas=str_replace("FROM","",$tablas);//Eliminamos clausula
	}
	if (strpos($tablas,'WHERE') !== false){//Comprobamos si tiene clausula WHERE
			$tablas=strstr($tablas,"WHERE",true);//Extraemos solo la tabla
	}
	//print "trataSubSelect: $tablas";
	return $tablas;
}


//Inserta registros en las tablas de agrupaciones
function insAgrupacion ($tdestino,$conexion,$nombreFichero,$tabla, $permiso, $localizador) {
	//print "insAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "INSERT INTO $tdestino (tabla,nombreFichero, permiso, localizador) 
	VALUES ('$tabla', '$nombreFichero','$permiso', '$localizador')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysql_error" ;
	}
}


//Borra registros en la tabla agrupaci�n
function delAgrupacion ($conexion,$nombreFichero,$tabla,$permiso) {
	//print "delAgrupacion";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$consulta = "DELETE FROM agrupacion where tabla='$tabla' and permiso='$permiso'";
	$cursor=mysqli_query($conexion,$consulta);
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysql_error" ;
	}
}

//Separamos las tablas en las celdas
function separaTablas($conexion)
{
//print "separaTablas";
$consulta="SELECT tabla,permiso,nombreFichero,localizador FROM agrupacion group by tabla,permiso";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor vseparaTablas"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = $datos[0];
			$permiso=$datos[1];
			$nombreFichero=$datos[2];
			$localizador=$datos[3];
			$elements = explode(",", $tabla);
				for ($ii = 0; $ii < count($elements); $ii++) {
				$nquotes = trim($elements[$ii]);//Elimina espacios al comienzo y al final
				if (strpos($nquotes," AS ") !== false) {//Si tiene clausula as
				$nquotes=strstr($nquotes ," AS ",true);}//Eliminamos la clausula as
				if (strpos($nquotes," ")!== false) {//Si tiene seudonimo
				$nquotes=strstr($nquotes," ",true);}//Eliminamos seudonimo
				insAgrupacion ("agrupacion_tmp",$conexion,$nombreFichero,$nquotes,$permiso,$localizador);
				}
			
		}	
	}
	
}

//Separamos las tablas en las celdas
function agrupPermisos($conexion,$nombreFichero)
{
//print "agrupPermisos";
$consulta="SELECT distinct tabla FROM agrupacion_tmp";
$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
	print "Error cursor agrupPermisos"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla = $datos[0];
			//print "tabla=$tabla";
			$localizador=obtLocTabla($conexion,$tabla);
			$permiso=obtPermTabla($conexion,$tabla);
			insAgrupacion ("agrup_perm",$conexion,$nombreFichero,$tabla,$permiso,$localizador);//Inserta el registro
			}
			
		}	
}

//Obtiene los localizadores de cada tabla
function obtLocTabla($conexion,$tabla){
//print "obtLocTabla";
$consulta="SELECT DISTINCT localizador FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtLocTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$localizador=$datos[0];
		}
	}
	return $localizador;
}
	
//Obtiene los permisos de cada tabla
function obtPermTabla($conexion,$tabla)
{
//print "obtPermTabla";
$consulta="SELECT DISTINCT permiso FROM agrupacion_tmp where tabla='$tabla'";
$cursor=mysqli_query($conexion,$consulta);
$tabla_aux=null;
	if(!$cursor) {
	print "Error cursor obtPermTabla"; }
	else {
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			if ($permiso!=""){
			$permiso=$permiso.",".$datos[0];}
			else{
			$permiso=$datos[0];}
		}
	}
	return $permiso;
}

function grabarBD ($conexion,$nombreFichero,$moduloNegocio, $componente, $alias) {
	//print "Grabar";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO localizador (nombreFichero,modulo_negocio, componente, alias) 
	VALUES ('$nombreFichero','$moduloNegocio', '$componente', '$alias')";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	//print "$consulta";
	if(mysqli_error($conexion)) {
	   print "1 if Grabar";	
	   print "mysql_error" ;
	}
}

function pintarDatos ($conexion,$nombreFichero) {
//print "pintar";
$consulta =  "SELECT tabla,permiso,localizador FROM agrup_perm WHERE nombreFichero='$nombreFichero' order by tabla";
	//print "$consulta";
	$cursor=mysqli_query($conexion,$consulta);
	if(!$cursor) {
		//print "alta";
	}
	else {
		cabeceraImportar();
		while($datos=mysqli_fetch_array($cursor)){
			$n=sizeof($datos);
			$tabla=$datos[0];
			$permiso=$datos[1];
			$localizador=$datos[2];
			echo "<td>$tabla</td>";
			echo "<td>$permiso</td>";
			echo "<td>$localizador</td>";
			echo "</tr>";
		}	
	}
	echo "</table>";
	echo "</table>";
	
}

function cabeceraImportar() {
	echo "<br>";
    echo "<table ALIGN = \"center\"  width=\"80%\" border=\"0\"><tr><td>";
    echo "<TABLE ALIGN = \"center\"  width=\"100%\" BORDER=\"1\">"
	.    "<thead>"
    .    "<TR>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
    .    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
    .    "</TR>"
    .    "</thead>"
	.    "<tbody>";

}

//print_r($data);
//print_r($data->formatRecords);
?>
