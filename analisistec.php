<html>
	<head>
		<title>Analisis Tecnico BKS</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="left-sidebar is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.html" id="logo">An�lisis T�cnico Banksphere</a></h1>
							</header>
						</div>

					<!-- Nav -->
							<nav id="nav">
							<ul>
								<li><a href="index.html">Home</a></li>
								<li>
									<a href="#">Opciones</a>
									<ul>
										<li>
											<a href="#">An�lisis T�cnico &hellip;</a>
											<ul>
												<li><a href="analisis.html">An�lisis t�cnico</a></li>
												<li><a href="analisis.html">Exportaci�n excell</a></li>
												<li><a href="ttpp.html">Ingesta ttpp</a></li>
											</ul>
										</li>
										<li><a href="analisislotes.html">AT Lotes Web Services</a></li>
										<li><a href="comppubli.html">Comparador publicaciones</a></li>
										<li><a href="permyloc.html">Permisos y Localizadores</a></li>
										<li><a href="descarga.html">Descarga / Instalaci�n</a></li>
									</ul>
								</li>
								<li><a href="analisis.html">Analisis T�cnico BKS</a></li>
							</ul>
						</nav>

				</div>


<?php 
//2016 - Manuel Llamas G�mez - Pool de implantaci�n
error_reporting (0);
ob_start();
session_start();
include("BaseDatos.php");
header("Content-Type: text/html;charset=utf-8_spanish_ci");


$nombre_archivo = $_FILES['archivoupload']['name']; 
$tipo_archivo = $_FILES['archivoupload']['type']; 
$tamano_archivo = $_FILES['archivoupload']['size']; 
$OP=$_POST['OP'];
$fichero=$_REQUEST[archivoupload];
$ficheroold=$_REQUEST[archivoold];
$ficheronew=$_REQUEST[archivonew];
$producto=$_REQUEST[producto];
$publicacion=$_REQUEST[publicacion];
$rfc=$_REQUEST[rfc];
$entrega=$_REQUEST[entrega];
$idpub=$_REQUEST[idpub];
$rijel=$_REQUEST[rijel];
$usabilidad=$_REQUEST[usabilidad];
$arq=$_REQUEST[arq];
$prop=$_REQUEST[prop];

/****************************************************************************************************/
?>

<?php

switch($OP) {
	case Analisis:
		pantallaanalisis($conexion);
		break;
	case AnalisisWS:
		pantallanalisisWS ($conexion);
		break;
	case CompararPub:
		pantallacompara($conexion);
		break;
	case PermisosTablas:
		pantallapermisos($conexion);
		break;
	case ActualizarTTPP:
		pantallattpp($conexion);
		break;
	case Permisos:
		$time=date("dmY-His");
		$nombreFichero="sql".$time.".xls";
		//$nombreFichero="sss.xls";
		$_SESSION['fichero']=$nombreFichero;
		pantallapermisos($conexion);
		if (move_uploaded_file($_FILES['archivoupload']['tmp_name'], $nombreFichero)){ 
          echo "El archivo ha sido cargado correctamente."; 
		}else{ 
            die ("Error al subir el fichero. Comprueba que se ha a�adido correctamente."); 
	   	}
		include("excel.php");
		break;
	case TablasParametros:
		$time=date("dmY-His");
		$nombreFichero="TTPP-".$time.".xls";
		$_SESSION[fichero]=$nombreFichero;
		pantallattpp($conexion);
		if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],$nombreFichero)){ 
          echo "El archivo se ha sido cargado correctamente."; 
		  cabeceraTTPP();
		}else{ 
            die ("Error al subir el fichero. Comprueba que se ha a�adido correctamente."); 
	   	}
		include("altaTTPP.php");
		break;
	case Localizador:
		pintarLoca($conexion);
		break;
	case GenerarWS:
		prepararficherosWS();
		break;
	case Generar:
		pantallaanalisis ($conexion);
		validarcampobl();
		prepararfichero();
		include("procesado.php");
		break;
	case Exportaexcell:
		prepficheroexcel();
		if (!$_SESSION['idioma']||$_SESSION['idioma']=='ES'){
			include("exportaexcell.php");
		}else{
			include("exportaexcellen.php");}
		break;
	case CompararPublicaciones:
		prepararficheros();
		pantallacompara($conexion);
		include("comparar.php");
		break;
	case OcultarDE:
		ocultarDE();
		break;
	case OcultarDI:
		ocultarDI();
		break;
	default:
		pantallaPrincipal();
		infoVersion();
		break;
}

function validarcampobl(){
$producto=$_REQUEST[producto];
$publicacion=$_REQUEST[publicacion];
if ($producto==""){
  die ("Deben rellenarse con un producto valido."); 
}
if ($publicacion==""){
  die ("Deben rellenarse con una publicaci�n valida."); 
}
}

function obtenerdatossgs3 ($idpub){
	$url="http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id=731678&j_username=Viewer&j_password=Viewer";
	//$urllogin="http://sgs.isban.gs.corp/sgs/Index.action";
	//login ($urllogin);
	$texto = file_get_contents($url);
	echo $texto;
}

function obtenerdatossgs2($idpub) {
$url = 'http://Viewer:Viewer@sgs.isban.gs.corp/sgs/PublicationInfo.action?id='.$idpub;
echo $url;
$page = file_get_contents($url);
echo $page;
}

function obtenerdatossgs4($idpub) {
$c = curl_init('http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id='.$idpub);
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
curl_setopt($c, CURLOPT_USERPWD, 'Viewer:Viewer');
$page = curl_exec($c);
curl_close($c);
echo $page;
}

function obtenerdatossgs6($idpub) {
$options = array('http' =>
    array(
        'method'  => 'GET',
        'header'  => 'Content-type: text/plain;charset=UTF-8\r\n'.
        'Referer: http://sgs.isban.gs.corp\r\n'.
        'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6\r\n'.
        'Cookie: user=Viewer; actividad=programacion;\r\n'
    )
);
$context = stream_context_create($options);
$url='http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id='.$idpub;
$page = file_get_contents($url, false, $context);
echo $page;
}

function obtenerdatossgs8($idpub){
$c = curl_init('http://sgs.isban.gs.corp/sgs/j_spring_security_check');
curl_setopt($c, CURLOPT_COOKIE, 'user=xIS07746');
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
echo $c;
$page = curl_exec($c);
curl_close($c);
echo $page;
}



function obtenerdatossgs12($idpub){
$ch = curl_init(); 

curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
curl_setopt($ch, CURLOPT_COOKIEJAR, "cookie.txt");
curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");
curl_setopt($ch, CURLOPT_URL, "http://sgs.isban.gs.corp/sgs/j_spring_security_check");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "j_username=Viewer&j_password=Viewer"); 
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects 
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable 

$res = curl_exec($ch);
echo $res;
// check $res here to see if login was successful

curl_setopt($ch, CURLOPT_URL, "http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id=".$idpub);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "newu=demo&pass=password");

$res = curl_exec($ch);
echo $res;
// check $res to see that the user was successfully created

curl_close($ch);
}

function obtenerdatossgs($idpub){
$cook=loginSGS('Viewer','Viewer',$idpub);
echo $cook;
}

function loginSGS($username, $password,$idpub) {

//$geoserverURL = "http://sgs.isban.gs.corp/sgs/j_spring_security_check.action";
$geoserverURL='http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id='.$idpub;


$post = http_build_query(array(
        "j_username" => $username,
        "j_password" => $password,
));

$context = stream_context_create(array("http"=>array(
    "method" => "POST",
    "header" => "Content-Type: application/x-www-form-urlencoded\r\n" .
            "Content-Length: ". strlen($post) . "\r\n",
    "content" => $post,
	"proxy" => 'tcp://172.31.219.30:8080',
    "request_fulluri" => true,
	//"Cookie: user=xIS07746;\r\n",
)));

$page = file_get_contents($geoserverURL, false, $context);
echo $page;

for($i = 0; $i < sizeof($http_response_header); $i++){

    $headerLine = $http_response_header[$i];

    $pos = strpos($headerLine, 'Set-Cookie');

    if ($pos === 0) {
            $str = explode("=",$headerLine);
            $value = explode(";",$str[1]);
            $cookieValue = $value[0];
            break;
    }

}

$cookieName = "JSESSIONID";
$cookieDomain = "http://localhost:8080";
$cookiePath = "/geoserver";
$cookieExpiration = 0;

setcookie($cookieName,$cookieValue,$cookieExpiration,$cookiePath);
return $cookieValue;
}

function obtenerdatossgs9($idpub){
$aContext = array(
    'http' => array(
        'proxy' => 'tcp://172.31.219.30:8080',
        'request_fulluri' => true,
		'Cookie: user=xIS07746;\r\n'
    ),
);
$cxContext = stream_context_create($aContext);
$sFile = file_get_contents('http://sgs.isban.gs.corp/sgs/PublicationInfo.action?id='.$idpub, False, $cxContext);
echo $sFile;
}

function login($url){
$username = 'Viewer';
$password = 'Viewer';
 
$context = stream_context_create(array(
    'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
));
echo $context;
$data = file_get_contents($url,false,$context);
echo $data;
}

//Prepara fichero para la generacion de an�lisis
function prepararfichero(){
	$time=date("dmY-His");//Recoge la fecha
	$nombreFichero="Analisis_".$time.".xml";//modifica el nombre del fichero con la fecha
	$_SESSION[fichero]=$nombreFichero;//Guardamos el nombre de los ficheros en la sesion
	if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],"tmp/".$nombreFichero)){ 
			
	}else{ 
		pantallaanalisis($conexion);
        die ("Error al subir el fichero. Comprueba que se ha a�adido correctamente."); 
	}
}



//Prepara fichero para la generacion de an�lisis
function prepficheroexcel(){
	$time=date("dmY-His");//Recoge la fecha
	$nombreFichero="Analisis_".$time.".xml";//modifica el nombre del fichero con la fecha
	$_SESSION[fichero]=$nombreFichero;//Guardamos el nombre de los ficheros en la sesion
	$producto=$_REQUEST[producto];
	$publicacion=$_REQUEST[publicacion];	
	$excel95 = isset($_POST['excel95']);
	$excel07 = isset($_POST['excel07']);
	
	$idioma = $_POST['cmblang'];
		
	//Guardamos el valor de los parametros de la sesion
	if (!$excel95)$excel95=0;
	if (!$excel07)$excel07=0;
		
	$_SESSION['envexcel']=true;//Para saber si el formulario a sido enviado
	$_SESSION['excel95']=$excel95;
	$_SESSION['excel07']=$excel07;
	$_SESSION['idioma']=$idioma;
	
	pantallaanalisis($conexion);
	
	//Validaciones
	if ($producto==""){
		die ("Deben rellenarse con un producto valido."); 
	}
	if ($publicacion==""){
		die ("Deben rellenarse con una publicaci�n valida."); 
	}
	
	if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],"tmp/".$nombreFichero)){ 
			
	}else{ 
		//pantallaanalisis($conexion);
        die ("Error al subir el fichero. Comprueba que se ha a�adido correctamente."); 
	}
		
	if ($excel95==0&&$excel07==0){
	die ("Debes seleccionar una opcion para mostrar.");
	}

}

//Prepara los ficheros para la comparacion
function prepararficherosWS(){
	pantallanalisisWS($conexion);
	if (isset($_FILES['fichero_usuario'])){
	
	$cantidad= count($_FILES["fichero_usuario"]["tmp_name"]);
	//echo $cantidad;
	
	for($i=0;$i<$cantidad;$i++){
		$time=date("dmY-His");//Recoge la fecha
		$nomFich="fichero".$i."".$time.".xml";//Nombre del fichero 
		//Guardamos el nombre de los ficheros en la sesion
		$_SESSION['fichero']=$nomFich;
		//Subimos el fichero al servidor
	if (move_uploaded_file($_FILES['fichero_usuario']["tmp_name"][$i],"tmp/".$nomFich)){ 
			//echo $nomFich;
	}else{ 
		pantallanalisisWS($conexion);
        die ("Error al subir el fichero. Comprueba que se ha a�adido correctamente."); 
	}
	//Extraemos el xml
	$xml = simplexml_load_file("tmp/".$nomFich);
	//Nombre del ens
	$nomens=$xml->assembly['name'];//Nombre del ens		
	echo"<td bgcolor=\"#EEECDC\"><b>Ensamblado:</b></font></td><td>".$nomens."</td></tr>";
	
	$numwsexp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
	$numwscon=count($xml->webServices->webServiceStates->webServiceState);//Numero de ws consumidos
		
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Expuestos</b></font></td></tr></table>";
	$nuWsExp=count($xml->channelAdapters->channelAdapter);
		
	if ($nuWsExp>0){//Si tiene ws
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Adaptador</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Fachada</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
		//filas
		foreach($xml->channelAdapters->channelAdapter as $channelAdapter) {
		$adaptador=$channelAdapter->adapterName;
		$tipo=$channelAdapter->type;
		$fachada=$channelAdapter->facadeName;
		$alias=$channelAdapter->alias;
		echo "<tr>";
		echo "<td>$adaptador</td>";
		echo "<td>$tipo</td>";
		echo "<td>$fachada</td>";
		echo "<td>$alias</td>";
		echo "</tr>";}
		
		echo "</tbody></table></tr></td></table>";
			
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}
	
	echo"<table><tr><td></td></tr></table><br/>";
		


//WS Expuestos

	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Consumidos</b></font></td></tr></table>";
		
	$nuWsCon=count($xml->webServices->webServiceStates->webServiceState);
		
	if ($nuWsCon>0){//Si tiene ws
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
			//filas
			foreach($xml->webServices->webServiceStates->webServiceState as $wsstates) {
			$aplicacion=$wsstates->application;
			$operacion=$wsstates->internalOP;
			$alias=$wsstates->alias;
			$tipo=$wsstates->transport;
			echo "<tr>";
			echo "<td>$aplicacion</td>";
			echo "<td>$operacion</td>";
			echo "<td>$alias</td>";
			echo "<td>$tipo</td>";
			echo "</tr>";}
		
		echo "</tbody></table></tr></td></table>";
		
	}else{//Si no tiene
		echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}
	
	echo"<table><tr><td></td></tr></table><br/>";	


	}

	
	}
	
}

//Prepara los ficheros para la comparacion
function prepararficheros(){

	if (isset($_FILES['fichero_usuario'])){
	
	$cantidad= count($_FILES["fichero_usuario"]["tmp_name"]);
		
	$time=date("dmY-His");//Recoge la fecha
	$nomFichOld="comp_old".$time.".xml";//Nombre del fichero antiguo
	$nomFichNew="comp_new".$time.".xml";//Nombre del fichero nuevo
	
	//Guardamos el nombre de los ficheros en la sesion
	$_SESSION['ficheronew']=$nomFichNew;
	$_SESSION['ficheroold']=$nomFichOld;
	
	//Subimos el fichero antiguo al servidor
	if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'][0],"tmp/".$nomFichOld)){ 
			
	}else{ 
		pantallacompara($conexion);
        die ("Error al subir el fichero Antiguo. Comprueba que se ha a�adido correctamente."); 
	}
	
	//Subimos el fichero nuevo al servidor
	if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'][1],"tmp/".$nomFichNew)){ 
			
	}else{ 
		pantallacompara($conexion);
        die ("Error al subir el fichero Nuevo. Comprueba que se ha a�adido correctamente."); 
	}}
	
	
	$chkmin = isset($_POST['chkmin']);
	$chkadd = isset($_POST['chkadd']);
	//Guardamos el valor de los checks en la sesion
	
	if (!$chkmin)$chkmin=0;
	if (!$chkadd)$chkadd=0;

	$_SESSION['chkmin']=$chkmin;
	$_SESSION['chkadd']=$chkadd;
	
	if ($chkmin==0&&$chkadd==0){
	pantallacompara($conexion);
	die ("Debes seleccionar una opcion para mostrar.");
	}
	
}

function pantallaanalisis ($conexion) {
	pantallaPrincipal();
	$producto=$_POST[producto];
	$publicacion=$_POST[publicacion];
	$rfc=$_POST[rfc];
	$entrega=$_POST[entrega];
	$fichero=$_POST[archivoupload];
	$rijel=$_POST[rijel];
	$usabilidad=$_POST[usabilidad];
	$arq=$_POST[arq];
	$prop=$_POST[prop]; 
	
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8_spanish_ci\" />";
	echo "<meta http-equiv=\"Content-type\" content=\"text/html\"; charset=\"utf-8\"/>";
	
	echo"<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>"
	."<td><font SIZE=2><b><u>INSTRUCCIONES:</u></b></font></td></tr>"
	."<tr><td><font SIZE=2><b>Generar An�lisis</b></font></td></tr>"
	."<tr><td><font SIZE=2>1.Rellenamos los datos obligatorios y si queremos los datos opcionales (Datos Entrega y Datos Instalaci�n).</font></td></tr>"
	."<tr><td><font SIZE=2>2.Obtenemos el fichero configuration.xml de la pagina de Detalle de la publicaci�n->Configuraci�n en el icono de Descargar <a href='Instrucciones.docx'> Ver detalle </a></font></td></tr>"
	."<tr><td><font SIZE=2>3.Pulsamos el bot�n 'Generar An�lisis Excel'.</font></td></tr>"
	."</tr></table></td></table>";
	echo "<script language=javascript>"
	."function comprueba_extension(formulario, archivo) { "
	."formulario.fiche.value=archivo;"
	."url=\"asistentes.php?OP=Importar\";"
	."window.location.href=url;"
	."}"
	."</script>";	
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\">"
	."<tr><td WIDTH=\"6%\"><font color = \"FFFFFF\">(*)Fichero:</font></td>"
	."<td><input type=\"file\" name=\"archivoupload\" size=\"100\" value=\"$fichero\"><font color = \"FFFFFF\" size=1> * Campo Obligatorio</font></td>"
	."<td><input type=\"hidden\" name=\"fiche\" value=$fiche></td>"
	."<td WIDTH=\"30%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>Generador de An�lisis T�cnico V01R07 </font></td></tr>"
	."<tr><td><font color = \"FFFFFF\">(*)Producto:</font></td><td><input type=\"text\" name=\"producto\" value=\"$producto\"></td></tr>"
	."<tr><td><font color = \"FFFFFF\">(*)Publicaci�n:</font></td><td colspan=3 border=1><input type=\"text\" name=\"publicacion\" size=\"70\" value=$publicacion></td></tr>"
	."<tr><td><font color = \"FFFFFF\" size=1></font></td></tr>";
	
	echo "<style type='text/css'>"
    .".elementoVisible {display:block;}"
    .".elementoOculto {display:none;}"
    .".linkContraido {"
    .".cursor: pointer;"
    ."background: #color url(direccionURL_imagenContraido) no-repeat;"
    ."width: anchopx;"
    ."[propiedades de los textos]}"
    .".linkExpandido {"
    ."cursor: pointer;"
    ." background: #color url(direccionURL_imagenExpandido) no-repeat;"
    ."width: anchopx;"
    ."[propiedades de los textos]}"
    ."</style>";

	
	
    echo "<script type='text/JavaScript'>"
    ."function desplegarContraer(cual,desde) {"
    ."      var elElemento=document.getElementById(cual);"
    ."     if(elElemento.className == 'elementoVisible') {"
    ."          elElemento.className = 'elementoOculto';"
    ."         desde.className = 'linkContraido';"
    ."   } else {"
    ."      elElemento.className = 'elementoVisible';"
    ."     desde.className = 'linkExpandido';}}"
    ."</script>";
	
	echo "<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\">"
	."<tr><td><font color = \"FFFFFF\" size=1> Campos Opcionales - Click para Expandir/Contraer</font></td></tr>"
    ."<tr><td><div onclick=\"desplegarContraer('datose',this);\" class=\"linkContraido\"><font color = \"FFFFFF\"><b>Datos Entrega </b></font></div></td></tr>"
	."<tr><td><div id=\"datose\" class='elementoOculto'><table>"
	."<tr><td><font color = \"FFFFFF\">Entrega:</font></td><td><input type=\"text\" name=\"entrega\" size=\"100\" value=$entrega></td></tr>"
	."<tr><td><font color = \"FFFFFF\">RFC:</font></td><td><input type=\"text\" name=\"rfc\" value=$rfc></td></tr></table></div></td></tr>"
	
	."<tr><td><div onclick=\"desplegarContraer('datosi',this);\" class=\"linkContraido\"><font color = \"FFFFFF\"><b>Datos Instalaci�n </b></font></div></td></tr>"
	."<tr><td><div id=\"datosi\" class='elementoOculto'><table>"
	."<tr><td><font color = \"FFFFFF\">Versi�n Rigel:</font></td><td><input type=\"text\" name=\"rijel\" value=\"$rijel\"></td></tr>"
	."<tr><td><font color = \"FFFFFF\">Glob. Usabilidad:</font></td><td colspan=3 border=1><input type=\"text\" name=\"usabilidad\" value=$usabilidad></td></tr>"
	."<tr><td><font color = \"FFFFFF\">Glob. Arquitectura:</font></td><td><input type=\"text\" name=\"arq\" value=$arq></td></tr>"
	."<tr><td valign=\"top\"><font color = \"FFFFFF\">Properties:</font></td><td><textarea name=\"prop\" value=$prop rows=5 cols=50></textarea></td></tr>"
	."</td></tr></table></div></td></tr>";
	
	echo "</table>";
	
	//Formato de salida
	echo "<form action=\"index.php\" method=\"post\">" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\"><tr><td><font color = \"FFFFFF\">Formato salida:</font>";
	
	if (!$_SESSION['envexcel']){
	echo "<input type=\"checkbox\" name=\"excel95\" checked><font color = \"FFFFFF\">Excel 95</font>";
	echo "<input type=\"checkbox\" name=\"excel07\" checked><font color = \"FFFFFF\">Excel 2007</font>";
	}else{
		if ($_SESSION['excel95']){
		echo "<input type=\"checkbox\" name=\"excel95\" checked><font color = \"FFFFFF\">Excel 95</font>";
		}else{
		echo "<input type=\"checkbox\" name=\"excel95\"><font color = \"FFFFFF\">Excel 95</font>";
		}
		if ($_SESSION['excel07']){
		echo "<input type=\"checkbox\" name=\"excel07\" checked><font color = \"FFFFFF\">Excel 2007</font>";
		}else {
		echo "<input type=\"checkbox\" name=\"excel07\"><font color = \"FFFFFF\">Excel 2007</font>";
		}	
	
	}
	
		
	//Seleccion de idioma
	echo "</tr></td><tr><td><font color = \"FFFFFF\">Idioma:</font><select name=\"cmblang\">"; 
	
	if ($_SESSION['idioma']=='EN'){
	echo "<option value=\"ES\" >Castellano</option><option value=\"EN\" selected=\"selected\">Ingles</option></select>";
	}else{
	echo "<option value=\"ES\" selected=\"selected\">Castellano</option><option value=\"EN\" >Ingles</option></select>";
	}
	echo"</tr></td>";
	
	echo"<table border=\"0\" bgcolor=\"#930101\" WIDTH=\"100%\"><tr><td>"
	
	//Botones	
	."<button type=\"submit\" name=\"OP\" value=\"Exportaexcell\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Generar An�lisis Excel </font></div>"
	."</button>"
	
		
	."<button type=\"submit\" name=\"OP\" VALUE=\"Generar\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Previsualizar </font></div>"
	."</button>"
	
	."</td></tr></table></form>";
	
	echo "</table>";
}

function pantallanalisisWS ($conexion) {
	pantallaPrincipal();
	
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8_spanish_ci\" />";
	echo "<meta http-equiv=\"Content-type\" content=\"text/html\"; charset=\"utf-8\"/>";
	
	echo"<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>"
	."<td><font SIZE=2><b><u>INSTRUCCIONES:</u></b></font></td></tr>"
	."<tr><td><font SIZE=2><b>Generar An�lisis</b></font></td></tr>"
	."<tr><td><font SIZE=2>1.Obtenemos los ficheros configuration.xml de la pagina de Detalle de la publicaci�n->Configuraci�n en el icono de Descargar <a href='Instrucciones.docx'> Ver detalle </a></font></td></tr>"
	."<tr><td><font SIZE=2>2.Pulsamos el bot�n 'Ejecutar'.</font></td></tr>"
	."</tr></table></td></table>";
	echo "<script language=javascript>"
	."function comprueba_extension(formulario, archivo) { "
	."formulario.fiche.value=archivo;"
	."url=\"asistentes.php?OP=Importar\";"
	."window.location.href=url;"
	."}"
	."</script>";	
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\">"
	."<tr><td WIDTH=\"6%\"><font color = \"FFFFFF\">(*)Ficheros:</font></td>"
	."<td><input name=\"fichero_usuario[]\" type=\"file\" size=\"100\" value=\"$fichero\" multiple><font color = \"FFFFFF\" size=1> * Campo Obligatorio</font></td>"
	."<td WIDTH=\"30%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>Generador de An�lisis T�cnico V01R07 </font></td></tr>"
	."<tr><td><font color = \"FFFFFF\" size=1></font></td></tr>";
	
	
	//Botones	
	echo "<tr><td><button type=\"submit\" name=\"OP\" VALUE=\"GenerarWS\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Ejecutar </font></div>"
	."</button>"
	
	."</td></tr></table></form>";
	
	echo "</table>";
}


function pantallacompara ($conexion) {
	pantallaPrincipal();
	$ficheroant=$_POST[archivoant];
	$ficheronue=$_POST[archivonue];
	
	echo"<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>"
	."<td><font SIZE=2><b><u>INSTRUCCIONES:</u></b></font></td></tr>"
	."<tr><td><font SIZE=2><b>Comparar Publicaciones</b></font></td></tr>"
	."<tr><td><font SIZE=2>1.Obtenemos los ficheros configuration.xml de la pagina de Detalle de la publicaci�n->Configuraci�n en el icono de Descargar <a href='Instrucciones.docx'> Ver detalle </a></font></td></tr>"
	."<tr><td><font SIZE=2>2.Es muy aconsejable renombrar los ficheros con _old y _new para no equivocarse.</font></td></tr>"
	."<tr><td><font SIZE=2>3.Pulsamos el bot�n 'Comparar Publicaciones'.</font></td></tr>"
	."</tr></table></td></table>";
	echo "<script language=javascript>"
	."function comprueba_extension(formulario, archivo) { "
	."formulario.fiche.value=archivo;"
	."url=\"asistentes.php?OP=Importar\";"
	."window.location.href=url;"
	."}"
	."</script>";	
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\">"
	."<tr><td WIDTH=\"8%\"><font color = \"FFFFFF\">Publicacion Antigua:</font></td>"
	."<td><input type=\"file\" name=\"fichero_usuario[]\" size=\"100\" ><font color = \"FFFFFF\" size=1> * Campo Obligatorio</font></td>"
	."<td><input type=\"hidden\" name=\"archivoant\" value=$ficheroant></td>"
	."<td WIDTH=\"30%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>Comparador Publicaciones V01R00 </font></td></tr>";
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\">"
	."<tr><td WIDTH=\"8%\"><font color = \"FFFFFF\">Publicacion Nueva:  </font></td>"
	."<td><input type=\"file\" name=\"fichero_usuario[]\" size=\"100\" ><font color = \"FFFFFF\" size=1> * Campo Obligatorio</font></td>"
	."<td><input type=\"hidden\" name=\"ficheronew\" value=$ficheronew></td>"
	."<td WIDTH=\"30%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2></font></td></tr></table>";
	echo "<form action=\"index.php\" method=\"post\">" 
    ."<table border=\"0\" bgcolor=\"#B60000\" WIDTH=\"100%\"><tr><td><font color = \"FFFFFF\">Mostrar:</font>";
	
	
	
	if ($_SESSION['chkadd']||!$_SESSION['enviado']){
	echo "<input type=\"checkbox\" name=\"chkadd\" checked><font color = \"FFFFFF\">Objetos A�adidos</font>";
	}else{
	echo "<input type=\"checkbox\" name=\"chkadd\"><font color = \"FFFFFF\">Objetos A�adidos</font>";
	}
	
	if ($_SESSION['chkmin']||!$_SESSION['enviado']){
	echo "<input type=\"checkbox\" name=\"chkmin\" checked><font color = \"FFFFFF\">Objetos Eliminados</font>";
	}else {
	echo "<input type=\"checkbox\" name=\"chkmin\"><font color = \"FFFFFF\">Objetos Eliminados</font>";
	}
	
	echo "</tr></td></form></table></table>";
	
	echo"<table bgcolor=\"#930101\" WIDTH=\"100%\"><tr><td>"
	
	."<button type=\"submit\" name=\"OP\" value=\"CompararPublicaciones\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Comparar Publicaciones </font></div>"
	."</button>"
	
	."</td></tr></table>";
	
	echo "</table>";
}

function pantallapermisos ($conexion) {
	pantallaPrincipal();
	echo "<script language=javascript>"
	."function comprueba_extension(formulario, archivo) { "
	."formulario.fiche.value=archivo;"
	."url=\"asistentes.php?OP=Importar\";"
	."window.location.href=url;"
	."}"
	."</script>";
	echo"<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>"
	."<td><font SIZE=2><b><u>INSTRUCCIONES:</u></b></font></td></tr>"
	."<tr><td><font SIZE=2><b>Obtener Permisos Localizadores</b></font></td></tr>"
	."<tr><td><font SIZE=2>1.Descargar la plantilla <a href='plantilla.xls'>aqui</a>.</font></td></tr>"
	."<tr><td><font SIZE=2>2.En la pesta�a 'localizadores', copiaremos los localizadores del SGS y en la pesta�a SQL se copiaran las consultas, despues subiremos el fichero y pulsamos el bot�n 'Procesar'.</font></td></tr>"
	."</tr></table></td></table>";	
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#C40317\" WIDTH=\"100%\">"
	."<tr><td><font color = \"FFFFFF\">Fichero:</font></td>"
	."<td><input type=\"file\" name=\"archivoupload\" ></td>"
	."<td><input type=\"hidden\" name=\"fiche\" value=$fiche></td>"
	."<td WIDTH=\"50%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>Busqueda de Permisos y Localizadores de Tablas V01R01</font></td></tr></table>&nbsp";
	
	echo"<table bgcolor=\"#930101\" WIDTH=\"100%\"><tr><td>"
	."<button type=\"submit\" name=\"OP\" VALUE=\"Permisos\" onclick=\"comprueba_extension(this.form, this.form.archivoupload.value)\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<font color = \"FFFFFF\">Consultar Permisos</font></button></td></tr></table>";
	
}


function pantallattpp ($conexion) {
	pantallaPrincipal();
	echo "<script language=javascript>"
	."function comprueba_extension(formulario, archivo) { "
	."formulario.fiche.value=archivo;"
	."url=\"asistentes.php?OP=Importar\";"
	."window.location.href=url;"
	."}"
	."</script>";	
	echo"<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>"
	."<td><font SIZE=2><b><u>INSTRUCCIONES:</u></b></font></td></tr>"
	."<tr><td><font SIZE=2><b>Cargar tablas de parametros en inventario</b></font></td></tr>"
	."<tr><td><font SIZE=2>1.Descargar la plantilla <a href='ttpp.xls'>aqui</a>.</font></td></tr>"
	."<tr><td><font SIZE=2>2.En la pesta�a 'ttpp', copiaremos las ttpp con la version y el localizador y pulsamos el bot�n 'TablasParametros'.</font></td></tr>"
	."</tr></table></td></table>";
	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table border=\"0\" bgcolor=\"#C40317\" WIDTH=\"100%\">"
	."<td><font color = \"FFFFFF\">Fichero: </font>"
	."<input type=\"file\" name=\"archivoupload\" >"
	."<input type=\"hidden\" name=\"fiche\" value=$fiche>"
	." <button type=\"submit\" name=\"OP\" VALUE=\"TablasParametros\" onclick=\"comprueba_extension(this.form, this.form.archivoupload.value)\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<font color = \"FFFFFF\">Actualizar TTPP</font></button></td>"
	."<td WIDTH=\"50%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>Inventario de Tablas de Par�metros V01R01</font></td></tr></table>&nbsp";
	
	
	
}


/****************************************************************************************************/

function pantallaPrincipal() {
    //print "antes func $codigo";

	echo "<form action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\" >" 
    ."<table bgcolor=\"#930101\" WIDTH=\"100%\"><tr><td>"
	."<button type=\"submit\" name=\"OP\" VALUE=\"Analisis\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">An�lisis T�cnico desde fichero</font></div>"
	//."<input type=\"image\" src='Icons/play4.png' alt=\"botongenerar\" width=\"24\" height=\"24\" background-color:\"transparent\"/>"
	."</button>"
	
	//Generador multiple
	."<button type=\"submit\" name=\"OP\" VALUE=\"AnalisisWS\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Analisis Tecnico Lotes WS</font></div>"
	//."<input type=\"image\" src='Icons/play4.png' alt=\"botongenerar\" width=\"24\" height=\"24\" background-color:\"transparent\"/>"
	."</button>"
	
	//Comparador de publicaciones
	."  <button type=\"submit\" name=\"OP\" value=\"CompararPub\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Comparador Publicaciones</font></div>"
//	."<input type=\"image\" src='Icons/export2.png' alt=\"botoncomparar\" width=\"24\" height=\"24\"/>"
	."</button>"
	
	."  <button type=\"submit\" name=\"OP\" value=\"PermisosTablas\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Permisos y Localizadores de Tablas</font></div>"
//	."<input type=\"image\" src='Icons/export2.png' alt=\"botonexportar\" width=\"24\" height=\"24\"/>"
	."</button>"
	
	."  <button type=\"submit\" name=\"OP\" value=\"ActualizarTTPP\" height=\"24\" style=\"background-color:'#B60000'\">"
	."<div align=\"center\"><font color = \"FFFFFF\">Actualizar Tablas de Par�metros</font></div>"
//	."<input type=\"image\" src='Icons/export2.png' alt=\"botonexportar\" width=\"24\" height=\"24\"/>"
	."</button>"
	
	."&nbsp&nbsp<a target=\"_blank\" href=\"http://nebula.isban.dev.corp/Nebula/login/index.jsp\"><font color = \"FFFFFF\" SIZE=2><u>Acceso a Nebula</u></font></a>"
	
	."&nbsp&nbsp<a target=\"_blank\"  href=\"http://sgs.isban.gs.corp/sgs/jsp/core/login.jsp?error=0\"><font color = \"FFFFFF\" SIZE=2><u>Acceso a  SGS</u></font></a></td>"
	
	. "<td WIDTH=\"20%\" align=\"right\"><font color = \"F0EEE0\" SIZE=2>SE Quality Assurance - System Config./ Implementation V01R07</font></td></tr></table>";

	
	}

function infoVersion(){
	echo "<br>";
	//V01R07
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R07 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo "Nueva funcionalidad Analisis Tecnico Lotes WS para obtener los ws expuestos y consumidos de varias aplicaciones a la vez a partir de su xml";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R06
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R06 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo "Compatibilidad con la aplicaci�n xamp del software distribuido";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R05
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R05 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo " -Nueva version del generador de analisis (V01R04): Se a�ade la opci�n del idioma de salida de excell entre Ingles y Castellano.";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R04
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R04 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo "  -Nueva funcionalidad Comparador de publicaciones (V01R00).";
	echo "<br>";
	echo "  -Gestion en directorio temporal y borrado automatico de ficheros temporales.";
	echo "<br>";
	echo "  -Almacenamiento de ficheros excel en directorio Analisis Tecnicos en funcion del delivery.";
	echo "<br>";
	echo "  -Posibilidad de escoger el formato excel de salida entre excel 95 o 2007.";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R03
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R03 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo " -Nueva version del generador de analisis en excel.";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R02
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R02 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo " -Se corrigen errores y se a�aden nuevas opciones.";
	echo "<br>";
	echo "</table></table>";
	echo "<br>";
	//V01R01
	echo "<table border=\"1\" WIDTH=\"100%\"><td><table border=\"0\" bgcolor=\"#EEECDC\" WIDTH=\"100%\"><tr>";
	echo "<font color = \"1C1563\" SIZE=3 ALIGN = \"center\" ><b>V01R01 - Cambios respecto a la anterior version:</b></font>";
	echo "<br>";
	echo " -Primera version del generador de analisis.";
	echo "<br>";
	echo "<br>";
	echo "</table></table>";
}

function cabeceraTTPP() {
echo "<br>";

	echo "<br>";
    echo "<table ALIGN = \"center\"  width=\"80%\" border=\"0\"><tr><td>";
    echo "<TABLE ALIGN = \"center\"  width=\"100%\" BORDER=\"1\">"
	.    "<thead>"
    .    "<TR>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
    .    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
    .    "</TR>"
    .    "</thead>"
	.    "<tbody>";

}
ob_end_flush();
?>
