<?php ob_start();
//2016 - Manuel Llamas G�mez - Pool de implantaci�n
header('Pragma: public'); 
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past    
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
header('Pragma: no-cache'); 
header('Expires: 0'); 
header('Content-Transfer-Encoding: none'); 
header('Content-Type: application/vnd.ms-excel'); // This should work for IE & Opera 
header('Content-type: application/x-msexcel'); // This should work for the rest 
header('Content-Disposition: attachment; filename="AnalisisTecnico"'. gmdate('d m Y H:i:s') .'.xls"');

$nombreFichero = $_SESSION[fichero]; //Nombre del fichero xml
$xml = simplexml_load_file($nombreFichero);
error_reporting(E_ALL ^ E_NOTICE);

//Generamos nombre para el fichero de proceso que sera indice
$time=date("dmY-His");//Recoge la fecha
$nomFichpro="procesado".$time.".xml";//modifica el nombre del fichero con la fecha

$numwsexp=count($xml->channelAdapters->channelAdapter);//Numero de ws expuestos
$numwscon=count($xml->webServices->webServiceStates->webServiceState);//Numero de ws consumidos
$numtrxop=count($xml->communication->trxOp->trxOpStates->trxOpState);//Numero de transacciones partenon
$numsat = count($xml->communication->sat->satStates->satState);//Numero de sat
$numtrxalt=count($xml->communication->altair->altairStates->altairState);//Numero de transacciones altair

pintarTitulo($xml->assembly['name']);
numcomp($conexion,$producto,$publicacion,$numwsexp,$numwscon,0,$numtrxop,$numsat,$numtrxalt);
datosGenerales($xml,$producto,$publicacion,$entrega,$rfc,$rijel,$usabilidad,$arq,$prop);//Datos Generales
wsExpuestos($xml);//Web services expuestos
wsConsumidos($xml);//Web services consumidos
tablasParametros($xml,$conexion,$nomFichpro,$publicacion);//Tablas de par�metros
trxOP($xml,$publicacion);//TrxOP
perLocalizadores($xml,$conexion,$doc,$nomFichpro,$publicacion);//Tabla localizadores
catalogoContenidos($xml);//Cat�logo de contenidos
pintarNumComp($consulta,$conexion,$publicacion);
deletetemp($conexion);

//Borra las tablas usadas para los permisos de localizadores
function deletetemp ($conexion) {
	//print "delete";
	$consulta = "TRUNCATE TABLE ttpp";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE ttpp_tmp";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	//print "delete";
	$consulta = "TRUNCATE TABLE localizador";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE consultas";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}
	
	$consulta = "TRUNCATE TABLE agrupacion";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE agrupacion_tmp";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE agrup_perm";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
	
	$consulta = "TRUNCATE TABLE publicacion";
	$cursor=mysql_query($consulta,$conexion);
	if(mysql_error()) {
	   print "1 if";	
	   print "mysql_error" ;
	}
	
	if(!$cursor) {
	    print "2 if";
		print "existe cursor";
		print "$consulta";
	}	
		

}

function numcomp($conexion,$producto,$publicacion,$numwsexp,$numwscon,$numttpp,$numtrxop,$numsat,$numtrxalt){
$consulta = "INSERT INTO publicacion (IDPUB,PRODUCTO,WSEXP,WSCON,TTPP,TRXOP,SAT,ALTAIR,PERMISOS) VALUES ('$publicacion','$producto',$numwsexp,$numwscon,$numttpp,$numtrxop,$numsat,$numtrxalt,0)";
$cursor=mysql_query($consulta,$conexion);
//print "$consulta";
//echo "<br/>";
if(mysql_error()) {
	   //print "La siguiente consulta no ha podido ser procesada:";
	   //echo "</br>";
	   //echo "$consulta";
	   //echo "</br>";
	   }
}

//Titulo
function pintarTitulo($nomens){
	echo"<br/>";
    echo "<font color = \"0B0B3B\" SIZE=5 ALIGN = \"center\" ><b>An�lisis T�cnico ".$nomens."</b></font>"; 
	echo"<br/>";
	echo"<br/>";
}

function pintarNumComp($consulta,$conexion,$publicacion){
$consulta = "SELECT * FROM publicacion WHERE IDPUB='$publicacion'";
$cursor=mysql_query($consulta,$conexion);
	if(!$cursor) {
		//print "alta";
	}
	else {
		while($datos=mysql_fetch_array($cursor)){
			$numwsexp = $datos[2];
			$numwscon = $datos[3];
			$numttpp = $datos[4];
			$numtrxop = $datos[5];
			$numsat = $datos[6];
			$numtrxalt = $datos[7];
			$numperm = $datos[8];
			}}

echo"<br/>";			
echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Inventario de componentes - ".$publicacion."</b></font></td></tr></table>";	
//Pintar tabla de numero de componentes
echo"<br/>";
echo"<table><tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Web Services Expuestos:</b></td><td>".$numwsexp."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Web Services Consumidos:</b></td><td>".$numwscon."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Tablas de par�metros:</b></td><td>".$numttpp."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Trx-Op Partenon:</b></td><td>".$numtrxop."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Sat:</b></td><td>".$numsat."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� de Trx Altair:</b></td><td>".$numtrxalt."</td></tr>"
."<td bgcolor=\"#EEECDC\"><b>N� tablas en comp. sql:</b></td><td>".$numperm."</td></tr>"
."</tr></table>";
echo"<br/>";
}

function perLocalizadores($xml,$conexion,$doc,$nomFichpro,$publicacion){
	
	$i=0;
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Permisos Localizadores</b></font></td></tr></table>";	
	$nuPermLoc=count($xml->sqlComponents->sqlComponent);
	//echo $nuPermLoc;	
	if ($nuPermLoc>0){//Si tiene componentes sql
		//Cabecera
		echo "<table ALIGN = \"left\"  width=\"80%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		. "<thead>"
		. "<TR>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
		. "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
		. "</TR>"
		. "</thead>"
		. "<tbody>";
				
		//filas
		foreach($xml->sqlComponents->sqlComponent as $sqlComponent) {
		//Tabla de localizadores
		$localizadores=$sqlComponent->parameters->sqlComponentParams;
		$module=$localizadores->module;
		$component=$localizadores->component;
		$dataSourceAlias=$localizadores->dataSourceAlias;
				
		//Insertamos en la tabla localizador
		grabarLoc($conexion,$nomFichpro,$module,$component,$dataSourceAlias);
		
			
		
		//Insertamos en la tabla consultas
		foreach($sqlComponent->sqls->{'sql-sentence'} as $sentence) {
			$tablas=$sentence->sql;
			$tablas = strtoupper($tablas);
			$module=$sentence->module;
			$component=$sentence->component;
			//echo $tablas;
			//echo"<br/>";
			grabarComp ($conexion,$nomFichpro,$module,$component,$tablas,$i);
			$i++;
		}
		}
		include("procesarPermisos.php");	

		$consulta =  "SELECT tabla FROM agrup_perm WHERE nombreFichero='$nomFichpro' order by tabla";
		//print "$consulta";
		$cursor=mysql_query($consulta,$conexion);
		$n=mysql_num_rows($cursor);
	
		$consulta = "UPDATE publicacion SET PERMISOS=$n WHERE IDPUB='$publicacion'";
		$cursor=mysql_query($consulta,$conexion);
		//print "$consulta";

		if(mysql_error()) {
		print "La siguiente consulta no ha podido ser procesada:";
		echo "</br>";
		echo "$consulta";
		echo "</br>";}	
					
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}

}

function grabarComp($conexion,$nombreFichero,$moduloNegocio, $componente, $tablas,$i) {
	
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	$tablas=trim($tablas);
	
	//$tablas = strtoupper($tablas);
    $tipoConsulta = substr($tablas, 0, 1);

	//Select
	if ($tipoConsulta=='S') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strripos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	//print "long+++$lonCadena";
	//print "where:$posWhere+++";
	//print "cadena:$tablas+++";
		if ($posWhere=="") {
		$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	//Insert
	if ($tipoConsulta=='I') {
		$permisos="INSERT";
		$posFrom = strpos($tablas, "INTO");
		$posWhere = strpos($tablas, "(");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("INTO", "", $tablasBBDD);
	}
	//Update
	if ($tipoConsulta=='U') {
		$permisos="UPDATE";
		$posFrom = strpos($tablas, "UPDATE");
		$posWhere = strpos($tablas, "SET");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
		$tablasBBDD = str_replace("UPDATE", "", $tablasBBDD);
	}
	//Delete
	if ($tipoConsulta=='D') {
		$permisos="DELETE";
		$posFrom = strpos($tablas, "FROM");
		//print "*A*$posFrom**";
		$posFrom=$posFrom+4;
		//print "*D*$posFrom**";
		$posWhere = strpos($tablas, "WHERE");
		$posWhere=$posWhere-$posFrom;
		$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	}
	
	//With temp
	if ($tipoConsulta=='W') {
	$permisos="SELECT";
    $lonCadena= -strlen($tablas);		 
    $posFrom = strpos($tablas, "FROM");
	//echo "</br>";
	//print "*A*$posFrom**";
	$posFrom=$posFrom+4;
	//print "*D*$posFrom**";
	$posWhere = strpos($tablas, "WHERE");// recupera el ultimo where de la cadena, por si hay union
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD = substr($tablas, $posFrom, $posWhere);
	//echo "$tablasBBDD";
		
	//Si hay un segundo from
	$tablasBBDD2 = substr($tablas, $posWhere,strlen($tablas));
	$posFrom = strpos($tablasBBDD2, "FROM");
	if ($posFrom!=""){
	$posFrom=$posFrom+4;
	$posWhere = strpos($tablasBBDD2, "WHERE");
	if ($posWhere=="") {$posWhere = strlen($tablas);}
	$posWhere=$posWhere-$posFrom;
	$tablasBBDD2 = substr($tablasBBDD2, $posFrom, $posWhere);
	}
	//echo "$tablasBBDD2";
	
	if ($tablasBBDD2!=""){
		$tablasBBDD=$tablasBBDD.",".$tablasBBDD2;}
	//$posAs=strpos($tablasBBDD, " AS");
	//$tablasBBDD = substr($tablasBBDD,0,$posAs);
	//echo "$tablasBBDD";
	//echo "</br>";
	}
	
	
	$tablasBBDD = str_replace("$", "", $tablasBBDD);
	$tablasBBDD = str_replace("SCHEMA.", "", $tablasBBDD);
	$tablasBBDD = str_replace("'", "", $tablasBBDD);
	if (trim($tablasBBDD)=="") {
			$tablasBBDD = $tablas;
	}
	
	//echo $tablasBBDD;
	//echo "</br>";
	//print "$i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos'";
	
	$consulta = "INSERT INTO consultas (fila,nombreFichero,modulo_negocio, componente, tablas,permiso) 
	VALUES ($i,'$nombreFichero','$moduloNegocio', '$componente', '$tablasBBDD','$permisos')";
	$cursor=mysql_query($consulta,$conexion);
	//print "$consulta";
	//echo "<br/>";
	if(mysql_error()) {
	   print "La siguiente consulta no ha podido ser procesada:";
	   echo "</br>";
	   echo "$tablasBBDD";
	   echo "</br>";
	}
}

//Tablas de par�metros
function tablasParametros($xml,$conexion,$nomFichpro,$publicacion){
		
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
		
	foreach($xml->global->treeTbls->treeTbl as $cacheadas) {
			$tabla=$cacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla cacheada:".$tabla;
			grabarTTPP($conexion,$nomFichpro,'ttpp_tmp',$tabla,'','');
			}
			
	//no cacheadas
	foreach($xml->global->mgrTables->mgrTable as $nocacheadas) {
			$tabla=$nocacheadas->table;
			$tabla=trim($tabla);
			if (strpos($tabla,'TablasParametros.') !== false){
				$tabla=str_replace("TablasParametros.","",$tabla);
			}
			//echo "Tabla no cacheada:".$tabla;
			grabarTTPP($conexion,$nomFichpro,'ttpp_tmp',$tabla,'','');
			
			}
	
	include("procesarTTPP.php");
		
    $consulta =  "SELECT TABLA,VERSION,LOCALIZADOR FROM ttpp WHERE nombreFichero='$nomFichpro' order by TABLA";
	//print "$consulta";
	$cursor=mysql_query($consulta,$conexion);
	$n=mysql_num_rows($cursor);
	
	$consulta = "UPDATE publicacion SET TTPP=$n WHERE IDPUB='$publicacion'";
	$cursor=mysql_query($consulta,$conexion);
	//print "$consulta";

	if(mysql_error()) {
	print "La siguiente consulta no ha podido ser procesada:";
	echo "</br>";
	echo "$consulta";
	echo "</br>";}	

 	
	
}

//Graba la ttpp
function grabarTTPP($conexion,$nomFichpro,$tdestino,$tabla,$version,$localizador) {
	//print "grabarTTPPIni";
	if(!$conexion) { //si no existe conexion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO $tdestino (TABLA,VERSION,LOCALIZADOR,nombreFichero) 
	VALUES ('$tabla', '$version', '$localizador','$nomFichpro')";
	//print "$consulta";
	$cursor=mysql_query($consulta,$conexion);
	
	if(mysql_error()) {
	  print "$consulta<br/>";
	  print "grabarTTPP-".$tdestino."-mysql_error<br/>";	
	}
}

	


//Graba la tabla inicial de localizadores
function grabarLoc($conexion,$nombreFichero,$moduloNegocio, $componente, $alias) {
	//print "Grabar";
	if(!$conexion) { //si no existe conecxion
		print"no estas conectado";
		exit;
	}
	
	$consulta = "INSERT INTO localizador (nombreFichero,modulo_negocio, componente, alias) 
	VALUES ('$nombreFichero','$moduloNegocio', '$componente', '$alias')";
	//print "$consulta";
	$cursor=mysql_query($consulta,$conexion);
	//print "$consulta";
	if(mysql_error()) {
	   print "grabarLoc-mysql_error";	
	}
}



//Datos Generales
function datosGenerales($xml,$producto,$publicacion,$entrega,$rfc,$rijel,$usabilidad,$arq,$prop){
	
	$nomens=$xml->assembly['name'];//Nombre del ens
	$confens=$xml->assembly['defaultBankChannel'];//Canal configuraci�n
	
	//Canal 
	foreach($xml->assembly->aebMultis->multisCategories as $multiscat){
	$gama=$multiscat->category[0];
	$idioma=$multiscat->category[1];
	$empresa=$multiscat->category[2];
	$canal=$multiscat->category[3];
	}
	
	//Comprobar auditoria
	$audit="No";
	foreach($xml->assembly->assemblyLogLevelsDefinitions->category as $category){
	$prioridad=$category->level;
	if ($prioridad='Audit'){
	$audit="Si";
	break;}
	}
	
	//Comprobar ws
	$ws="No";
	$nuWsExp=count($xml->channelAdapters->channelAdapter);
	$nuWsCon=count(count($xml->webServices->webServiceStates->webServiceState));
	if ($nuWsEx>0 or $nuWsCon>0){//No tiene
	$ws="Si";
	}
	
	
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Datos Entrega</b></font></td></tr></table>";	
	//Datos Entrega
	
	//Pintar tabla entrega
	echo"<table border=\"1\"><tr>"
	."<td bgcolor=\"#EEECDC\"><b>Nombre SGS:</b></td><td>".$producto."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Entrega PRE:</b></td><td>".$entrega."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Baseline:</b></td><td>".$publicacion."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Ensamblado:</b></font></td><td>".$nomens."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>RFC:</b></font></td><td>".$rfc."</td></tr>"
	."</tr></table>";
	echo"<br/>";
	
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Datos Instalaci�n</b></font></td></tr></table>";	
	//Datos Entrega
	$prop = str_replace("\n","<br>",$prop);
	//Pintar tabla entrega
	echo"<table border=\"1\"><tr>"
	."<td bgcolor=\"#EEECDC\"><b>Rigel:</b></td><td>".$rijel."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Globales Usabilidad:</b></td><td>".$usabilidad."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Globales Arquitectura:</b></td><td>".$arq."</td></tr>"
	."<td bgcolor=\"#EEECDC\" valign=\"top\"><b>Properties:</b></td><td>".$prop."</td></tr>"
	."</tr></table>";
	echo"<br/>";
	  
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Datos Generales</b></font></td></tr></table>";	
	//Datos Generales
	//Pintar tabla datos generales
	echo"<table border=\"1\"><tr>"
	."<td bgcolor=\"#EEECDC\"><b>Auditoria:</b></td><td>".$audit."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Configuracion:</b></td><td>".$confens."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Gama Perfil:</b></td><td>".$gama."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Idioma:</b></td><td>".$idioma."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Empresa:</b></td><td>".$empresa."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Canal Marco:</b></td><td>".$canal."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>WebServices:</b></td><td>".$ws."</td></tr>"
	."</tr></table>";
	echo"<br/>";
 }

//TrxOP Partenon
function trxOP($xml){
	//Partenon
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>TrxOP Partenon</b></font></td></tr></table>";		
	$numTrxOp=count($xml->communication->trxOp->trxOpStates->trxOpState);
		
	if ($numTrxOp>0){//Si tiene trx
	
	$protocolo=$xml->communication->trxOp->trxOpProtocol;
	$modo=$xml->communication->trxOp->trxOpDefaultMode;
	$redg=$xml->communication->trxOp->trxOpRedGProtocol;
	$tcpalias=$xml->communication->trxOp->trxOpTCPAlias;
	
	
	//Pintar tabla
	echo"<table border=\"1\"><tr>"
	."<td bgcolor=\"#EEECDC\"><b>Protocolo</b></font></td><td>".$protocolo."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>Modo:</b></td><td>".$modo."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>ProtocoloRedG:</b></td><td>".$redg."</td></tr>"
	."<td bgcolor=\"#EEECDC\"><b>TCP Alias:</b></td><td>".$tcpalias."</td></tr>"
	."</tr></table>";
    	
	
	//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacci�n</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operaci�n</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versi�n</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		echo"<br/>";
		
		//filas
		foreach($xml->communication->trxOp->trxOpStates->trxOpState as $trxOpState) {
		$aplicacion=$trxOpState->application;
		$oi=$trxOpState->internalOp;
		$trx=$trxOpState->transaction;
		$operacion=$trxOpState->operation;
		$version=$trxOpState->version;
		echo "<tr>";
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$operacion</td>";
		echo "<td>$version</td>";
		echo "</tr>";}
		
		echo "</tbody></table></td></tr></table><br/>";
				
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table><br/>";//No tiene
	}

	//Sat
	echo"<br/>";
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Sat</b></font></td></tr></table>";
		
	$kids = $xml->communication->sat->satStates->children();
	$numSat=count($kids);
	//echo $numSat;
	
	if ($numSat>0){//Si tiene trx
	
		$protsat=$xml->communication->sat->satProtocol;
		$aliassat=$xml->communication->sat->satAlias;
		$canallog=$xml->communication->sat->satLogicalChannels;
		$canalfis=$xml->communication->sat->satPyshicalChannels;
	
	
		//Pintar tabla
		echo"<table border=\"1\"><tr>"
		."<td bgcolor=\"#EEECDC\"><b>Protocolo</b></font></td><td>".$protsat."</td></tr>"
		."<td bgcolor=\"#EEECDC\"><b>Alias:</b></td><td>".$aliassat."</td></tr>"
		."<td bgcolor=\"#EEECDC\"><b>Canal L�gico:</b></td><td>".$canallog."</td></tr>"
		."<td bgcolor=\"#EEECDC\"><b>Canal F�sico:</b></td><td>".$canalfis."</td></tr>"
		."</tr></table>";
    	
	
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Nombre</th>"
		.    "<th WIDTH=\"15%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versi�n</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		echo"<br/>";
		
		//filas
		foreach($xml->communication->sat->satStates->satState as $satState) {
		$aplicacion=$satState->application;
		$oi=$satState->internalOp;
		$trx=$satState->satName;
		$version=$satState->version;
		
		if ($trx!=""){
		
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$version</td>";
		echo "</tr>";}}
		
		echo "</table>";
		echo "</table>";
		echo"<br/>";	
		echo"<br/>";
		
		
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	echo"<br/>";
	}
	
	//Altair
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>TrxOP Altair</b></font></td></tr></table>";
	
	$kids = $xml->communication->altair->altairStates->children();
	$numTrxAltair=count($kids);
	
	if ($numTrxAltair>0){//Si tiene trx
	
		$protaltair=$xml->communication->altair->altairProtocol;
		$altairalias=$xml->communication->altair->altairAlias;
	
	
		//Pintar tabla
		echo"<table border=\"1\"><tr>"
		."<td bgcolor=\"#EEECDC\"><b>Protocolo:</b></font></td><td>".$protaltair."</td></tr>"
		."<td bgcolor=\"#EEECDC\"><b>TCP Alias:</b></td><td>".$altairalias."</td></tr>"
		."</tr></table>";
    	
		
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicaci�n</th>"
		.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operacion Interna</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Transacci�n</th>"
		.    "<th WIDTH=\"20%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Versi�n</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
		
		
		//filas
		foreach($xml->communication->altair->altairStates->altairState as $altairState) {
		$aplicacion=$altairState->application;
		$oi=$altairState->internalOp;
		$trx=$altairState->transaction;
		$version=$altairState->version;
		
		echo "<td>$aplicacion</td>";
		echo "<td>$oi</td>";
		echo "<td>$trx</td>";
		echo "<td>$version</td>";
		echo "</tr>";}
		
		echo "</table>";
		echo "</table>";
		echo"<br/>";	
		echo"<br/>";
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	echo"<br/>";
	}
}



//WS Expuestos
function wsExpuestos($xml){

	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Expuestos</b></font></td></tr></table>";
	$nuWsExp=count($xml->channelAdapters->channelAdapter);
		
	if ($nuWsExp>0){//Si tiene ws
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Adaptador</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Fachada</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
		//filas
		foreach($xml->channelAdapters->channelAdapter as $channelAdapter) {
		$adaptador=$channelAdapter->adapterName;
		$tipo=$channelAdapter->type;
		$fachada=$channelAdapter->facadeName;
		$alias=$channelAdapter->alias;
		echo "<tr>";
		echo "<td>$adaptador</td>";
		echo "<td>$tipo</td>";
		echo "<td>$fachada</td>";
		echo "<td>$alias</td>";
		echo "</tr>";}
		
		echo "</tbody></table></tr></td></table>";
			
	}else{//Si no tiene
	echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}
	
	echo"<table><tr><td></td></tr></table><br/>";
		
}

//WS Expuestos
function wsConsumidos($xml){
	echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Web Services Consumidos</b></font></td></tr></table>";
		
	$nuWsCon=count($xml->webServices->webServiceStates->webServiceState);
		
	if ($nuWsCon>0){//Si tiene ws
		//Cabecera tabla
		echo "<table ALIGN = \"left\"  width=\"100%\" border=\"0\"><tr><td>";
		echo "<TABLE ALIGN = \"left\"  width=\"100%\" BORDER=\"1\">"
		.    "<thead>"
		.    "<TR>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Aplicaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Operaci�n</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Alias</th>"
		.    "<th WIDTH=\"25%\" bgcolor=\"#E8E6CD\" ALIGN = \"left\">Tipo</th>"
		.    "</TR>"
		.    "</thead>"
		.    "<tbody>";
				
			//filas
			foreach($xml->webServices->webServiceStates->webServiceState as $wsstates) {
			$aplicacion=$wsstates->application;
			$operacion=$wsstates->internalOP;
			$alias=$wsstates->alias;
			$tipo=$wsstates->transport;
			echo "<tr>";
			echo "<td>$aplicacion</td>";
			echo "<td>$operacion</td>";
			echo "<td>$alias</td>";
			echo "<td>$tipo</td>";
			echo "</tr>";}
		
		echo "</tbody></table></tr></td></table>";
		
	}else{//Si no tiene
		echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}
	
	echo"<table><tr><td></td></tr></table><br/>";	
}


function catalogoContenidos($xml){
	echo"<br/>";
		echo"<table WIDTH=\"100%\" bgcolor= \"0B0B3B\" border=1><tr bgcolor=\"#FFFFFF\"><td><font color = \"0B0B3B\"><b>Cat�logo de Contenidos</b></font></td></tr></table>";
	$catcon=$xml->other->catCon;
	if ($catcon=="no"){
		echo"<table><tr><td>No tiene.</td></tr></table>";//No tiene
	}else{
		echo"<table><tr><td>Si tiene.</td></tr></table>";//Si tiene}
}



}


?>