﻿<!DOCTYPE HTML>
<!--
	2019-DevSecOps Platform - SGT IT Delivery
	Manuel Llamas Gómez
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Permisos Localizadores</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>

<?php 
//2016 - Manuel Llamas Gómez - Pool de implantación
error_reporting (0);
ob_start();
session_start();
include("BaseDatos.php");
header("Content-Type: text/html;charset=utf-8_spanish_ci");


$nombre_archivo = $_FILES['archivoupload']['name']; 
$tipo_archivo = $_FILES['archivoupload']['type']; 
$tamano_archivo = $_FILES['archivoupload']['size']; 
$OP=$_POST['OP'];
$fichero=$_REQUEST[archivoupload];
$ficheroold=$_REQUEST[archivoold];
$ficheronew=$_REQUEST[archivonew];
$producto=$_REQUEST[producto];
$publicacion=$_REQUEST[publicacion];
$rfc=$_REQUEST[rfc];
$entrega=$_REQUEST[entrega];
$idpub=$_REQUEST[idpub];
$rijel=$_REQUEST[rijel];
$usabilidad=$_REQUEST[usabilidad];
$arq=$_REQUEST[arq];
$prop=$_REQUEST[prop];

/****************************************************************************************************/
switch($OP) {
	case PermisosTablas:
		pantallapermisos($conexion);
		break;
	case Permisos:
		cabecera();
		permisos();
		break;
	default:
		cabecera();
		pantallapermisos($conexion);
		piepagina();
		break;
}

function permisos(){
	if (validarfichero()){
		pantallapermisos($conexion);
		?>
		<script>desplegarContraer('datose',this);</script>
		<?php
		include("excel.php");
		//echo 'Vamos a borrar: '.$nombreFichero;
		//unlink ($nombreFichero);
	}else{
		pantallapermisos($conexion);
	}
	piepagina();
}

//Prepara fichero para la generacion de análisis
function validarfichero(){
	$time=date("dmY-His");
	$nombreFichero="SQL-".$time.".xls";
	//echo 'Nombre del fichero: '.$nombreFichero;
	$_SESSION['fichero']=$nombreFichero;
	if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],$nombreFichero)){ 
		return true;	
	}else{ 
		//pantallaanalisis($conexion);
        //die ("Error al subir el fichero. Comprueba que se ha añadido correctamente."); 
		echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">¡Error al subir el fichero! Comprueba que se ha añadido correctamente.</font></b>'
        .'</div>';
	}
}

function cabecera() {
	//Pinta el menu y la cabecera
?>   

	<body class="left-sidebar is-preload">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header">

					<!-- Inner -->
						<div class="inner">
							<header>
								<h1><a href="index.html" id="logo">Consulta permisos y localizadores tablas</a></h1>
							</header>
						</div>

				
							<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="index.html">Home</a></li>
								<li>
									<a href="#">Utilidades</a>
									<ul>
										<li>
											<a href="#">Análisis Técnico &hellip;</a>
											<ul>
												<li><a href="analisis.php">Análisis técnico</a></li>
												<li><a href="analisis.php">Exportación excell</a></li>
												<li><a href="ttpp.php">Ingesta ttpp</a></li>
											</ul>
										</li>
										<li><a href="analisislotes.php">AT Lotes Web Services</a></li>
										<li><a href="comppubli.php">Comparador publicaciones</a></li>
										<li><a href="permyloc.php">Permisos y Localizadores</a></li>
									</ul>
								</li>
								<li><a href="descarga.html">Descarga / Instalación</a></li>
							</ul>
						</nav>


				</div>



<?php
}





function pantallapermisos($conexion){
$producto=$_POST[producto];
	$publicacion=$_POST[publicacion];
	$rfc=$_POST[rfc];
	$entrega=$_POST[entrega];
	$fichero=$_POST[archivoupload];
	$rijel=$_POST[rijel];
	$usabilidad=$_POST[usabilidad];
	$arq=$_POST[arq];
	$prop=$_POST[prop]; 
	

?>
			<!-- Main -->
				<div class="wrapper style1">

					<div class="container">
					<div onclick=desplegarContraer('datose',this); class=linkContraido>---Click para Ocultar o Mostrar Buscador---</div>
						<div id=datose class='elementoVisible'>
						<div class="row gtr-200">
							<div class="col-4 col-12-mobile" id="sidebar">
								<hr class="first" />
															
								<section>
									<header>
										<h3><a href="#">Instrucciones de uso:</a></h3>
									</header>
									<p>
										Seguir los siguientes pasos para generar el informe:
									</p>
									<div class="row gtr-50">
										<div class="col-4">
											<a href="#" class="image fit"><img src="images/pic10.jpg" alt="" /></a>
										</div>
										<div class="col-8">
											<h4>1.Descargar plantilla</h4>
											<p>
												Descargar la plantilla <a href='plantilla.xls'>aqui</a>.
											</p>
										</div>
										<div class="col-4">
											<a href="#" class="image fit"><img src="images/pic11.jpg" alt="" /></a>
										</div>
										<div class="col-8">
											<h4>2.Actualizar plantilla</h4>
											<p>
												En la pestaña 'localizadores', copiaremos los localizadores del SGS y en la pestaña SQL se copiaran las consultas.
											</p>
										</div>
										<div class="col-4">
											<a href="#" class="image fit"><img src="images/pic12.jpg" alt="" /></a>
										</div>
										<div class="col-8">
											<h4>3.Generar informe</h4>
											<p>
												Subiremos el fichero modificado y pulsamos el botón 'Procesar'.
											</p>
										</div>
									</div>
									<footer>
										<a href="descarga.html" class="button">Instalación / Descargas</a>
									</footer>
								</section>
								<hr />
								
							</div>
							<div class="col-8 col-12-mobile imp-mobile" id="content">
								<article id="main">
									<header>
										<h3><a href="permyloc.php">Consulta permisos y localizadores tablas desde excel</a></h3>

									<section>
							
									<p>
														
										<script language=javascript>
										function comprueba_extension(formulario, archivo) { 
										formulario.fiche.value=archivo;
										url=\asistentes.php?OP=Importar\;
										window.location.href=url;
										};
										</script>
										
										<form action=permyloc.php method=post enctype=multipart/form-data > 
										<td><b>Campos Obligatorios</b></td>
										<table border=0 WIDTH=100%>
										<tr><td >(*)Fichero:</td>
										<td><input type=file name=archivoupload size=60 value=<?php echo $fichero ?>></td>
										<td><input type=hidden name=fiche value=<?php echo $fiche ?>></td>
										<tr><td><font size=1></font></td></tr>
										

	<style type='text/css'>
    .elementoVisible {display:block;}
    .elementoOculto {display:none;}
    .linkContraido {
    .cursor: pointer;
    background: #color url(direccionURL_imagenContraido) no-repeat;
    width: anchopx;
    [propiedades de los textos]}
    .linkExpandido {
    cursor: pointer;
     background: #color url(direccionURL_imagenExpandido) no-repeat;
    width: anchopx;
    [propiedades de los textos]}
    </style>

	
	
    <script type='text/JavaScript'>
    function desplegarContraer(cual,desde) {
          var elElemento=document.getElementById(cual);
         if(elElemento.className == 'elementoVisible') {
              elElemento.className = 'elementoOculto';
             desde.className = 'linkContraido';
       } else {
          elElemento.className = 'elementoVisible';
         desde.className = 'linkExpandido';}
		 }</script>
	
	
	
	</tr></td><table border=0 WIDTH=100%><tr><td>
	<button type=submit name=OP value=Permisos height=24 onclick="comprueba_extension(this.form, this.form.archivoupload.value)">
	<div align=center><font color = FFFFFF>Consultar Permisos</font></div>
	</button></td></tr></table></table></form>
				

									</p>
								</section>
								<section id="banner"></section>
								</header>
								</article>
							</div>
						</div>
						</div>
<?php
}
?>
<?php	
function piepagina (){
?>
						<hr />
						<div class="row">
							<article class="col-4 col-12-mobile special">
								<a href="descarga.html" class="image featured"><img src="images/descargas.png" alt="" /></a>
								<header>
									<h3><a href="descarga.html">Descagas / Instalación</a></h3>
								</header>
								<p>
									Acceder a todas las versiones anteriores de la aplicación
								</p>
							</article>
							<article class="col-4 col-12-mobile special">
								<a href="http://sgsl.isban.gs.corp/sgs/jsp/core/login.jsp?error=0" target="_blank" class="image featured"><img src="images/sgslogo.png" alt="" /></a>
								<header>
									<h3><a href="http://sgsl.isban.gs.corp/sgs/jsp/core/login.jsp?error=0" target="_blank">Portal SGS</a></h3>
								</header>
								<p>
									Portal de Sistema de Gestión de software
								</p>
							</article>
							<article class="col-4 col-12-mobile special">
								<a href="http://nebula.isban.dev.corp/Nebula/" target="_blank" class="image featured"><img src="images/nebula.gif" alt="" /></a>
								<header>
									<h3><a href="http://nebula.isban.dev.corp/Nebula/" target="_blank">Nebula</a></h3>
								</header>
								<p>
									Portal de gestion de ensamblados bks en cert.
								</p>
							</article>
						</div>
					</div>

				</div>

			<!-- Footer -->
				<div id="footer">
					<div class="container">
						<div class="row">

							<!-- Tweets -->
								<!-- Tweets -->
								<section class="col-4 col-12-mobile">
									<header>
										<h2 class="icon brands fa-twitter circled"><span class="label">Tweets</span></h2>
									</header>
									<ul class="divided">
										<li>
											<article class="tweet">
												Se libera la ultima versión de la aplicación V01R08
												<span class="timestamp">5 minutes ago</span>
											</article>
										</li>
										<li>
											<article class="tweet">
												Esto es de palo pero lo dejo porque queda bonito.
												<span class="timestamp">30 minutes ago</span>
											</article>
										</li>
								</ul>
								</section>

							<!-- Posts -->
								<section class="col-4 col-12-mobile">
									<header>
										<h2 class="icon solid fa-file circled"><span class="label">Posts</span></h2>
									</header>
									<ul class="divided">
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Se libera la ultima versión de la aplicación V01R08</a></h3>
												</header>
												<span class="timestamp">3 hours ago</span>
											</article>
										</li>
										<li>
											<article class="post stub">
												<header>
													<h3><a href="#">Esto es de palo pero lo dejo porque queda bonito.</a></h3>
												</header>
												<span class="timestamp">6 hours ago</span>
											</article>
										</li>
									</ul>
								</section>

							<!-- Photos -->
								<section class="col-4 col-12-mobile">
									<header>
										<h2 class="icon solid fa-camera circled"><span class="label">Photos</span></h2>
									</header>
									<div class="row gtr-25">
										<div class="col-6">
											<a href="#" class="image fit"><img src="images/pic10.jpg" alt="" /></a>
										</div>
										<div class="col-6">
											<a href="#" class="image fit"><img src="images/pic11.jpg" alt="" /></a>
										</div>
										<div class="col-6">
											<a href="#" class="image fit"><img src="images/pic12.jpg" alt="" /></a>
										</div>
										<div class="col-6">
											<a href="#" class="image fit"><img src="images/pic13.jpg" alt="" /></a>
										</div>

									</div>
								</section>

						</div>
						<hr />
						<div class="row">
							<div class="col-12">

								<!-- Contact -->
									<section class="contact">
										<header>
											<h3>Contacto</h3>
										</header>
										<p>Redes sociales y esas cosas.</p>
										<ul class="icons">
											<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
											<li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
											<li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
											<li><a href="#" class="icon brands fa-pinterest"><span class="label">Pinterest</span></a></li>
											<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
											<li><a href="#" class="icon brands fa-linkedin-in"><span class="label">Linkedin</span></a></li>
										</ul>
									</section>

								<!-- Copyright -->
									<div class="copyright">
										<ul class="menu">
											<li>&copy; Aplicación análisis técnico. All rights reserved.</li>
										</ul>
									</div>

							</div>

						</div>
					</div>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>

<?php
}



function cabeceraTTPP() {
echo "<br>";

	echo "<br>";
    echo "<table ALIGN = \"center\"  width=\"80%\" border=\"0\"><tr><td>";
    echo "<TABLE ALIGN = \"center\"  width=\"100%\" BORDER=\"1\">"
	.    "<thead>"
    .    "<TR>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Tablas</th>"
    .    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Permiso</th>"
	.    "<th WIDTH=\"30%\" bgcolor=\"#E8E6CD\">Localizador</th>"
    .    "</TR>"
    .    "</thead>"
	.    "<tbody>";

}
ob_end_flush();

			
				


function validarcampobl(){
$producto=$_REQUEST[producto];
$publicacion=$_REQUEST[publicacion];
if ($producto==""){
 //echo '<script type="text/javascript">alert("Debe introducir un producto válido");</script>';
 //die ("Deben rellenarse con un producto valido"); 
  echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">¡Debe introducir un producto SGS válido!</font></b>'
        .'</div>';
}
else if ($publicacion==""){
 //die ("Deben rellenarse con una publicación valida.");
//echo '<script type="text/javascript">alert("Debe introducir una instancia de publicación válida");</script>';
echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">¡Debe introducir una instancia de publicación SGS válida!</font></b>'
        .'</div>';
}
}


//Prepara fichero para la generacion de análisis
function prepararfichero(){
	$time=date("dmY-His");//Recoge la fecha
	$nombreFichero="Analisis_".$time.".xml";//modifica el nombre del fichero con la fecha
	$_SESSION[fichero]=$nombreFichero;//Guardamos el nombre de los ficheros en la sesion
	if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],"tmp/".$nombreFichero)){ 
			
	}else{ 
		//pantallaanalisis($conexion);
        //die ("Error al subir el fichero. Comprueba que se ha añadido correctamente."); 
		echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">¡Error al subir el fichero! Comprueba que se ha añadido correctamente.</font></b>'
        .'</div>';
	}
}



//Prepara fichero para la generacion de análisis
function prepficheroexcel(){
	$time=date("dmY-His");//Recoge la fecha
	$nombreFichero="Analisis_".$time.".xml";//modifica el nombre del fichero con la fecha
	$_SESSION[fichero]=$nombreFichero;//Guardamos el nombre de los ficheros en la sesion
	$producto=$_REQUEST[producto];
	$publicacion=$_REQUEST[publicacion];	
	$excel95 = isset($_POST['excel95']);
	$excel07 = isset($_POST['excel07']);
	
	$idioma = $_POST['cmblang'];
		
	//Guardamos el valor de los parametros de la sesion
	if (!$excel95)$excel95=0;
	if (!$excel07)$excel07=0;
		
	$_SESSION['envexcel']=true;//Para saber si el formulario a sido enviado
	$_SESSION['excel95']=$excel95;
	$_SESSION['excel07']=$excel07;
	$_SESSION['idioma']=$idioma;
	
	//pantallaanalisis($conexion);
	

	
	if (move_uploaded_file($_FILES['archivoupload']['tmp_name'],"tmp/".$nombreFichero)){ 
			
	
		
	if ($excel95==0&&$excel07==0){
		echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">Debes seleccionar un formato de salida válido.</font></b>'
        .'</div>';
	}
	
	else{
		echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">Ficheros guardados en <a href=\"file://Analisis Tecnicos\">C:\xampp\htdocs\isban\Analisis Tecnicos</a></font></b>'
        .'</div>';
	}

	}else{ 
		//pantallaanalisis($conexion);
        echo '<div class="msg" align=center>'
        .'<b><font color=\"#FA1B3C\">¡Error al subir el fichero! Comprueba que se ha añadido correctamente.</font></b>'
        .'</div>';
	}
}

//Prepara los ficheros para la comparacion
function prepararficheros(){

	if (isset($_FILES['fichero_usuario'])){
	
	$cantidad= count($_FILES["fichero_usuario"]["tmp_name"]);
		
	$time=date("dmY-His");//Recoge la fecha
	$nomFichOld="comp_old".$time.".xml";//Nombre del fichero antiguo
	$nomFichNew="comp_new".$time.".xml";//Nombre del fichero nuevo
	
	//Guardamos el nombre de los ficheros en la sesion
	$_SESSION['ficheronew']=$nomFichNew;
	$_SESSION['ficheroold']=$nomFichOld;
	
	//Subimos el fichero antiguo al servidor
	if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'][0],"tmp/".$nomFichOld)){ 
			
	}else{ 
		pantallacompara($conexion);
        die ("Error al subir el fichero Antiguo. Comprueba que se ha añadido correctamente."); 
	}
	
	//Subimos el fichero nuevo al servidor
	if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'][1],"tmp/".$nomFichNew)){ 
			
	}else{ 
		pantallacompara($conexion);
        die ("Error al subir el fichero Nuevo. Comprueba que se ha añadido correctamente."); 
	}}
	
	
	$chkmin = isset($_POST['chkmin']);
	$chkadd = isset($_POST['chkadd']);
	//Guardamos el valor de los checks en la sesion
	
	if (!$chkmin)$chkmin=0;
	if (!$chkadd)$chkadd=0;

	$_SESSION['chkmin']=$chkmin;
	$_SESSION['chkadd']=$chkadd;
	
	if ($chkmin==0&&$chkadd==0){
	pantallacompara($conexion);
	die ("Debes seleccionar una opcion para mostrar.");
	}
	
}
?>